<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.5.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser-json-array</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png8</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../assets/textures/player.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>0.5</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0,0</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../assets/player/player_down_01.png</key>
            <key type="filename">../assets/player/player_down_02.png</key>
            <key type="filename">../assets/player/player_down_03.png</key>
            <key type="filename">../assets/player/player_down_04.png</key>
            <key type="filename">../assets/player/player_down_05.png</key>
            <key type="filename">../assets/player/player_down_06.png</key>
            <key type="filename">../assets/player/player_down_07.png</key>
            <key type="filename">../assets/player/player_down_08.png</key>
            <key type="filename">../assets/player/player_down_09.png</key>
            <key type="filename">../assets/player/player_down_10.png</key>
            <key type="filename">../assets/player/player_down_11.png</key>
            <key type="filename">../assets/player/player_down_12.png</key>
            <key type="filename">../assets/player/player_down_13.png</key>
            <key type="filename">../assets/player/player_down_14.png</key>
            <key type="filename">../assets/player/player_down_15.png</key>
            <key type="filename">../assets/player/player_down_16.png</key>
            <key type="filename">../assets/player/player_down_17.png</key>
            <key type="filename">../assets/player/player_down_18.png</key>
            <key type="filename">../assets/player/player_down_19.png</key>
            <key type="filename">../assets/player/player_down_20.png</key>
            <key type="filename">../assets/player/player_down_21.png</key>
            <key type="filename">../assets/player/player_jump_01.png</key>
            <key type="filename">../assets/player/player_jump_02.png</key>
            <key type="filename">../assets/player/player_jump_03.png</key>
            <key type="filename">../assets/player/player_jump_04.png</key>
            <key type="filename">../assets/player/player_jump_05.png</key>
            <key type="filename">../assets/player/player_jump_06.png</key>
            <key type="filename">../assets/player/player_jump_07.png</key>
            <key type="filename">../assets/player/player_jump_08.png</key>
            <key type="filename">../assets/player/player_jump_09.png</key>
            <key type="filename">../assets/player/player_jump_10.png</key>
            <key type="filename">../assets/player/player_jump_11.png</key>
            <key type="filename">../assets/player/player_jump_12.png</key>
            <key type="filename">../assets/player/player_jump_13.png</key>
            <key type="filename">../assets/player/player_push_01.png</key>
            <key type="filename">../assets/player/player_push_02.png</key>
            <key type="filename">../assets/player/player_push_03.png</key>
            <key type="filename">../assets/player/player_push_04.png</key>
            <key type="filename">../assets/player/player_push_05.png</key>
            <key type="filename">../assets/player/player_push_06.png</key>
            <key type="filename">../assets/player/player_push_07.png</key>
            <key type="filename">../assets/player/player_push_08.png</key>
            <key type="filename">../assets/player/player_push_09.png</key>
            <key type="filename">../assets/player/player_push_10.png</key>
            <key type="filename">../assets/player/player_push_11.png</key>
            <key type="filename">../assets/player/player_push_12.png</key>
            <key type="filename">../assets/player/player_push_13.png</key>
            <key type="filename">../assets/player/player_push_14.png</key>
            <key type="filename">../assets/player/player_turn_01.png</key>
            <key type="filename">../assets/player/player_turn_02.png</key>
            <key type="filename">../assets/player/player_turn_03.png</key>
            <key type="filename">../assets/player/player_turn_04.png</key>
            <key type="filename">../assets/player/player_turn_05.png</key>
            <key type="filename">../assets/player/player_turn_06.png</key>
            <key type="filename">../assets/player/player_turn_07.png</key>
            <key type="filename">../assets/player/player_turn_08.png</key>
            <key type="filename">../assets/player/player_turn_09.png</key>
            <key type="filename">../assets/player/player_turn_10.png</key>
            <key type="filename">../assets/player/player_turn_11.png</key>
            <key type="filename">../assets/player/player_turn_12.png</key>
            <key type="filename">../assets/player/player_turn_13.png</key>
            <key type="filename">../assets/player/player_turn_14.png</key>
            <key type="filename">../assets/player/player_turn_15.png</key>
            <key type="filename">../assets/player/player_turn_16.png</key>
            <key type="filename">../assets/player/player_turn_17.png</key>
            <key type="filename">../assets/player/player_turn_18.png</key>
            <key type="filename">../assets/player/player_turn_19.png</key>
            <key type="filename">../assets/player/player_turn_20.png</key>
            <key type="filename">../assets/player/player_turn_21.png</key>
            <key type="filename">../assets/player/player_walk_01.png</key>
            <key type="filename">../assets/player/player_walk_02.png</key>
            <key type="filename">../assets/player/player_walk_03.png</key>
            <key type="filename">../assets/player/player_walk_04.png</key>
            <key type="filename">../assets/player/player_walk_05.png</key>
            <key type="filename">../assets/player/player_walk_06.png</key>
            <key type="filename">../assets/player/player_walk_07.png</key>
            <key type="filename">../assets/player/player_walk_08.png</key>
            <key type="filename">../assets/player/player_walk_09.png</key>
            <key type="filename">../assets/player/player_walk_10.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>85,138,170,275</rect>
                <key>scale9Paddings</key>
                <rect>85,138,170,275</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../assets/player/player_push_15.png</key>
            <key type="filename">../assets/player/player_push_16.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>86,138,171,276</rect>
                <key>scale9Paddings</key>
                <rect>86,138,171,276</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../assets/player/player_push_17.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>85,138,170,276</rect>
                <key>scale9Paddings</key>
                <rect>85,138,170,276</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../assets/player/player_push_18.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>86,138,171,275</rect>
                <key>scale9Paddings</key>
                <rect>86,138,171,275</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../assets/player_01/boxing_01.png</key>
            <key type="filename">../assets/player_01/boxing_02.png</key>
            <key type="filename">../assets/player_01/boxing_06.png</key>
            <key type="filename">../assets/player_01/boxing_07.png</key>
            <key type="filename">../assets/player_01/boxing_10.png</key>
            <key type="filename">../assets/player_01/boxing_16.png</key>
            <key type="filename">../assets/player_01/boxing_21.png</key>
            <key type="filename">../assets/player_01/boxing_22.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>112,134,224,267</rect>
                <key>scale9Paddings</key>
                <rect>112,134,224,267</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../assets/player_01/boxing_03.png</key>
            <key type="filename">../assets/player_01/boxing_04.png</key>
            <key type="filename">../assets/player_01/boxing_05.png</key>
            <key type="filename">../assets/player_01/boxing_08.png</key>
            <key type="filename">../assets/player_01/boxing_09.png</key>
            <key type="filename">../assets/player_01/boxing_17.png</key>
            <key type="filename">../assets/player_01/boxing_18.png</key>
            <key type="filename">../assets/player_01/boxing_20.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>112,133,224,267</rect>
                <key>scale9Paddings</key>
                <rect>112,133,224,267</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../assets/player_01/boxing_11.png</key>
            <key type="filename">../assets/player_01/boxing_12.png</key>
            <key type="filename">../assets/player_01/boxing_15.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>112,135,224,270</rect>
                <key>scale9Paddings</key>
                <rect>112,135,224,270</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../assets/player_01/boxing_13.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>112,136,224,271</rect>
                <key>scale9Paddings</key>
                <rect>112,136,224,271</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../assets/player_01/boxing_14.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>112,135,224,269</rect>
                <key>scale9Paddings</key>
                <rect>112,135,224,269</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../assets/player_01/boxing_19.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>112,134,224,268</rect>
                <key>scale9Paddings</key>
                <rect>112,134,224,268</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../assets/player_01/next_level_01.png</key>
            <key type="filename">../assets/player_01/next_level_02.png</key>
            <key type="filename">../assets/player_01/next_level_03.png</key>
            <key type="filename">../assets/player_01/next_level_04.png</key>
            <key type="filename">../assets/player_01/next_level_05.png</key>
            <key type="filename">../assets/player_01/next_level_06.png</key>
            <key type="filename">../assets/player_01/next_level_07.png</key>
            <key type="filename">../assets/player_01/next_level_08.png</key>
            <key type="filename">../assets/player_01/next_level_09.png</key>
            <key type="filename">../assets/player_01/next_level_10.png</key>
            <key type="filename">../assets/player_01/next_level_11.png</key>
            <key type="filename">../assets/player_01/next_level_12.png</key>
            <key type="filename">../assets/player_01/next_level_13.png</key>
            <key type="filename">../assets/player_01/next_level_14.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,138,200,275</rect>
                <key>scale9Paddings</key>
                <rect>100,138,200,275</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../assets/player_01/remote_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,134,199,267</rect>
                <key>scale9Paddings</key>
                <rect>100,134,199,267</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../assets/player_01/remote_02.png</key>
            <key type="filename">../assets/player_01/remote_03.png</key>
            <key type="filename">../assets/player_01/remote_04.png</key>
            <key type="filename">../assets/player_01/remote_05.png</key>
            <key type="filename">../assets/player_01/remote_06.png</key>
            <key type="filename">../assets/player_01/remote_07.png</key>
            <key type="filename">../assets/player_01/remote_08.png</key>
            <key type="filename">../assets/player_01/remote_09.png</key>
            <key type="filename">../assets/player_01/remote_10.png</key>
            <key type="filename">../assets/player_01/remote_11.png</key>
            <key type="filename">../assets/player_01/remote_12.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,134,199,268</rect>
                <key>scale9Paddings</key>
                <rect>100,134,199,268</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../assets/player</filename>
            <filename>../assets/player_01</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
