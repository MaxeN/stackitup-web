var game = new Phaser.Game(1920, 1080, Phaser.AUTO, '', {
    preload: preload
});
var debug = true;
function preload() {
    game.load.crossOrigin = 'anonymous';
    game.load.baseURL = './';

    game.state.add('Loading', GameManager.Loading);
    game.state.add('Intro', GameManager.Intro);
    game.state.add('Menu', GameManager.Menu);
    game.state.add('HowTo', GameManager.HowTo);
    game.state.add('NewGame', GameManager.NewGame);
    game.state.add('Game', GameManager.Game);
    game.state.add('Score', GameManager.Score);
    game.state.add('Leaderboard', GameManager.Leaderboard);

    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
    game.scale.pageAlignHorizontally = true;
    game.scale.pageAlignVertically = true;
    game.scale.trackParentInterval = 1;

    game.add.plugin(PhaserInput.Plugin);


    game.state.start('Loading');
}

GameManager.Loading.prototype = {
    startingScreen: 'Intro',
    preload: function () {


    var progressBar = game.add.graphics();
    var progressBox = game.add.graphics();
    progressBox.beginFill(0x222222, 0.8);
    progressBox.drawRect(0, 0, 320, 50);
    progressBox.endFill();
    progressBox.position.set(game.world.centerX - progressBox.width / 2, game.world.centerY - progressBox.height / 2);
    progressBar.beginFill(0xffffff, 1);
    progressBar.drawRect(0, 0, 310, 40);
    progressBar.endFill();
    progressBar.position.set(game.world.centerX - progressBar.width / 2, game.world.centerY - progressBar.height / 2);
    progressBar.scale.x = 0;

    var loadingText = game.add.text(game.world.centerX, game.world.centerY + 50, 'Loading...', {
        font: '20px monospace',
        fill: '#ffffff'
    });

    var percentText = game.add.text(game.world.centerX, game.world.centerY, '0%', {
        font: '18px monospace',
        fill: '#ffffff'
    });
    loadingText.anchor.set(0.5, 0.5);
    percentText.anchor.set(0.5, 0.5);
    var lastValue = {count: 0};
    game.load.onFileComplete.add(function (progress) {
        game.add.tween(progressBar.scale).to({x: progress / 100}, 150, Phaser.Easing.Linear.None, true)
        game.add.tween(lastValue).to({count: progress}, 150, Phaser.Easing.Linear.None, true).onUpdateCallback﻿(function () {
            percentText.setText(lastValue.count.toFixed(0) + '%');
        }, this);
    });
    game.load.onLoadComplete.add(function () {
        setTimeout(function () {
            progressBar.scale.x = 1;
            percentText.setText('100%');
        }, 200);
        game.add.tween(progressBar).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true, 500).onComplete.add(function () {
            progressBox.destroy();
        });
        game.add.tween(progressBox).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true, 500).onComplete.add(function () {
            progressBox.destroy();
        });
        game.add.tween(percentText).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true, 500).onComplete.add(function () {
            percentText.destroy();
        });
        game.add.tween(loadingText).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true, 500).onComplete.add(function () {
            loadingText.destroy();
        });
    });
    game.load.image('background', './assets/textures/background.png');
    game.load.image('background_base', './assets/textures/background_base.png');
    game.load.image('background_car', './assets/textures/background_car.png');
    game.load.image('background_food', './assets/textures/background_food.png');
    game.load.image('background_furniture', './assets/textures/background_furniture.png');
    game.load.image('background_plane', './assets/textures/background_plane.png');
    game.load.image('background_ship', './assets/textures/background_ship.png');
    game.load.image('logo', './assets/textures/logo.png');
    game.load.atlas('textures', './assets/textures/textures.png', './assets/textures/textures.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
    game.load.atlas('textures_01', './assets/textures/textures_01.png', './assets/textures/textures_01.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
    game.load.atlas('player', './assets/textures/player.png', './assets/textures/player.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
    game.load.atlas('playerGameOver', './assets/textures/playerGameOver.png', './assets/textures/playerGameOver.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
    game.load.atlas('leaderboard', './assets/textures/leaderboard.png', './assets/textures/leaderboard.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
    game.load.atlas('textures_02', './assets/textures/textures_02.png', './assets/textures/textures_02.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
    game.load.atlas('textures_03', './assets/textures/textures_03.png', './assets/textures/textures_03.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
    game.load.atlas('textures_04', './assets/textures/textures_04.png', './assets/textures/textures_04.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
    game.load.atlas('player_01', './assets/textures/player_01.png', './assets/textures/player_01.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);
    game.load.atlas('eq', './assets/textures/eq.png', './assets/textures/eq.json', Phaser.Loader.TEXTURE_ATLAS_JSON_HASH);

    game.load.audio('intro', ['./assets/audio/intro.mp3']);
    game.load.audio('tape', ['./assets/audio/ducttape.mp3']);
    game.load.audio('lightOn', ['./assets/audio/lightOn.mp3']);
    game.load.audio('lightOff', ['./assets/audio/lightOff.mp3']);
    game.load.audio('menuIn', ['./assets/audio/menuIn.mp3']);
    game.load.audio('musicBackground', ['./assets/audio/musicBackground.mp3']);
    game.load.audio('angels', ['./assets/audio/angels.mp3']);
    game.load.audio('hit', ['./assets/audio/hit.mp3']);
    game.load.audio('jump', ['./assets/audio/jump.mp3']);
    game.load.audio('jump_of', ['./assets/audio/jump_of.mp3']);
    game.load.audio('push', ['./assets/audio/push.mp3']);
    game.load.audio('walk', ['./assets/audio/walk.mp3']);
    game.load.audio('countup', ['./assets/audio/countup.mp3']);
    game.load.audio('countEnd', ['./assets/audio/countEnd.mp3']);
    game.load.audio('boxDrop', ['./assets/audio/boxDrop.mp3']);
    game.load.audio('boxSmash', ['./assets/audio/boxSmash.mp3']);
    game.load.audio('fart', ['./assets/audio/fart.mp3']);
    game.load.audio('swear', ['./assets/audio/swear.mp3']);
    game.load.audio('chain', ['./assets/audio/chainSound.mp3']);
    game.load.audio('explosion', ['./assets/audio/explosion.mp3']);
    game.load.audio('lawnmower', ['./assets/audio/lawnmower.mp3']);
    game.load.audio('box_mow_0', ['./assets/audio/box_mow_0.mp3']);
    game.load.audio('box_mow_1', ['./assets/audio/box_mow_1.mp3']);
    game.load.audio('springs', ['./assets/audio/springs.mp3']);
    game.load.audio('musicGame_0', ['./assets/audio/musicGame_0.mp3']);
    game.load.audio('musicGame_1', ['./assets/audio/musicGame_1.mp3']);
    game.load.audio('musicGame_2', ['./assets/audio/musicGame_2.mp3']);
    game.load.audio('musicGame_3', ['./assets/audio/musicGame_3.mp3']);
    game.load.audio('musicGame_4', ['./assets/audio/musicGame_4.mp3']);
    game.load.audio('item', ['./assets/audio/item.mp3']);
    game.load.audio('points', ['./assets/audio/points.mp3']);


    game.load.bitmapFont('font_score', './assets/fonts/font.png', './assets/fonts/font.xml');
    game.load.bitmapFont('led_font', './assets/fonts/led_font.png', './assets/fonts/led_font.xml');

    game.load.video('intro', './assets/mp4/intro.mp4');
},
    create: function () {

        // initialize the brainCloud client library here with our game id, secret, and game version
        _bc = new BrainCloudWrapper();
        _bc.initialize("12565", "7ecfd6f5-1eab-4ac9-b1e5-2e612c335849", "1.0.0");


        var self = this;
        setTimeout(function () {
            createBackgroundMusic();
            play = game.add.sprite(game.world.width / 2, game.world.height / 2, 'textures', 'boxPlay');
            play.anchor.set(0.5, 0.5);
            play.inputEnabled = true;
            play.input.useHandCursor = true;

            function onPressPlay() {

                play.events.onInputDown.remove(onPressPlay);
                play.inputEnabled = false;
                var graphics = game.add.graphics();
                graphics.beginFill(0x000000);
                graphics.drawRect(0, 0, game.world.width, game.world.height);
                graphics.endFill();
                graphics.alpha = 0;
                game.add.tween(graphics).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true).onComplete.add(function () {
                    game.state.start(self.startingScreen);
                });


            }

            play.events.onInputDown.add(onPressPlay, this);
        }, 1000);

    },
};

