var GameManager = {};

GameManager.Loading = function (game) {
};
GameManager.Intro = function (game) {
};
GameManager.Menu = function (game) {
};
GameManager.HowTo = function (game) {
};
GameManager.NewGame = function (game) {
};
GameManager.Game = function (game) {
};
GameManager.Score = function (game) {
};
GameManager.Leaderboard = function (game) {
};
var _bc;
var userName = generateName();
var currentScore = 0;

var music;
var mute = false;
var audioList = [];
var createBackgroundMusic = function () {
    music = game.add.audio('musicBackground');
    music.loop = true;
    music.volume = 0;
    music.allowMultiple = false;

};
var PlaySound = function (audio) {
    var stop = false;
    audioList.forEach(function (element) {
        if (element.key == audio.key) {
            if (element.allowMultiple == false) {
                stop = true;
            }
        }
    });
    if (stop) {
        return;
    }
    audio.mute = mute;
    audio.play();
    audioList.push(audio);
    audio.onStop.add(function () {
        audioList = audioList.filter(function (value, index, arr) {
            return value != audio;
        });
    }, this);
};

var setMute = function () {
    audioList.forEach(function (element) {
        element.mute = mute;
    });
};

var playBackgroundMusic = function () {
    stopTimeout = false;
    musicGame = game.add.audio("musicGame_" + currentMusicIndex);
    musicGame.volume = 1;
    musicGame.loop = false;
    musicGame.allowMultiple = false;
    PlaySound(musicGame);
    timeOutMusic = setTimeout(() => {
        if (stopTimeout) {
            return;
        }
        musicGame.stop();
        if (++currentMusicIndex > 4) {
            currentMusicIndex = 0;
        }
        this.playBackgroundMusic();
    }, musicGame.durationMS);

};
var stopBackgroundMusic = function () {
    stopTimeout = true;
    clearTimeout(timeOutMusic);
    game.add.tween(musicGame).to({volume: 0}, 4000, Phaser.Easing.Linear.None, true).onComplete.add(() => {
        musicGame.stop();
    });
};


var onlyUnique = function (value, index, self) {
    return self.indexOf(value) === index;
};
var getRandomInt = function (min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

var shuffleArray = function (array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
};

var distance = function (x1, y1, x2, y2) {
    return Math.hypot(x2 - x1, y2 - y1)
};

// Fragment shaders are small programs that run on the graphics card and alter
// the pixels of a texture. Every framework implements shaders differently but
// the concept is the same. This shader takes the lightning texture and alters
// the pixels so that it appears to be glowing. Shader programming itself is
// beyond the scope of this tutorial.
//
// There are a ton of good resources out there to learn it. Odds are that your
// framework already includes many of the most popular shaders out of the box.
//
// This is an OpenGL/WebGL feature. Because it runs in your web browser
// you need a browser that support WebGL for this to work.
Phaser
    .Filter.Glow = function (game) {
    Phaser.Filter.call(this, game);

    this.fragmentSrc = [
        "precision lowp float;",
        "varying vec2 vTextureCoord;",
        "varying vec4 vColor;",
        'uniform sampler2D uSampler;',

        'void main() {',
        'vec4 sum = vec4(0);',
        'vec2 texcoord = vTextureCoord;',
        'for(int xx = -4; xx <= 4; xx++) {',
        'for(int yy = -3; yy <= 3; yy++) {',
        'float dist = sqrt(float(xx*xx) + float(yy*yy));',
        'float factor = 0.0;',
        'if (dist == 0.0) {',
        'factor = 2.0;',
        '} else {',
        'factor = 2.0/abs(float(dist));',
        '}',
        'sum += texture2D(uSampler, texcoord + vec2(xx, yy) * 0.002) * factor;',
        '}',
        '}',
        'gl_FragColor = sum * 0.025 + texture2D(uSampler, texcoord);',
        '}'
    ];
};

Phaser.Filter.Glow.prototype = Object.create(Phaser.Filter.prototype);
Phaser.Filter.Glow.prototype.constructor = Phaser.Filter.Glow;


var debugLog = function (message, ...optionalParams) {
    if (debug) {
        console.log(message, optionalParams)
    }
};

var creditsTextValue = "Click anywhere to close" +
    "\n\n\n\n\n\n\n" +
    "\nGame Stack It Up! made by" +
    "\nKatarzyna 'UglyRevenge' Strzębicka" +
    "\nand" +
    "\nJakub 'SirMaxeN' Komar" +
    "\nWork together as BLUE BEAR team\n\n\n" +
    "\nGRAPHIC DESIGNER\n" +
    "\nKatarzyna Strzębicka\n\n\n" +
    "\nGAME DEV\n" +
    "\nJakub Komar\n\n\n\n" +
    "\nMUSIC AND SOUNDS\n" +
    "\nRoyalty Free Music from Bensound\n" +
    "\nFreesounds.org" +
    "\ncydon" +
    "\nKieranKeegan" +
    "\nIsaroru" +
    "\nTimbre" +
    "\nScrampunk" +
    "\nWingcommander1" +
    "\nqubodup" +
    "\njakobthiesen" +
    "\nProject_Trident" +
    "\nRossBell" +
    "\nAryaNotStark" +
    "\nlipalearning" +
    "\nspycrah" +
    "\nBenDrain" +
    "\nNakhas" +
    "\nrar2k" +
    "\nkhenshom" +
    "\ndiakunik" +
    "\nStrikeWhistler" +
    "\nMerrick079" +
    "\nBluePaint" +
    "\n1Kaylin_Dickson" +
    "\nShaneF91" +
    "\nsseletskyy" +
    "\npuZZelZ\n" +
    "\nFluffing a Duck Kevin MacLeod (incompetech.com)" +
    "\nLicensed under Creative Commons: By Attribution 3.0" +
    "\nhttp://creativecommons.org/licenses/by/3.0/\n" +
    "\nRadio Martini Kevin MacLeod (incompetech.com)" +
    "\nLicensed under Creative Commons: By Attribution 3.0" +
    "\nhttp://creativecommons.org/licenses/by/3.0/\n" +
    "\nCheerful Whistling by Free Music | https://soundcloud.com/fm_freemusic" +
    "\nLicensed under Creative Commons: By Attribution 3.0" +
    "\nhttps://creativecommons.org/licenses/by/3.0/\n" +
    "\nBye, Bye Brain! by Alexander Nakarada | https://www.serpentsoundstudios.com" +
    "\nCreative Commons Attribution 4.0 International (CC BY 4.0)" +
    "\nhttps://creativecommons.org/licenses/by/4.0/" +
    "\nMusic by Kinemesis_Music from Pixabay" +
    "\n\n\n\n\n" +
    "\nCODE PLUGINS\n" +
    "\nPhaser 2\n" +
    "\nPhaserInput by Azerion\n" +
    "\nGlow Filter by MatthewBarker\n" +
    "\nBrainCloud\n" +
    "\nName generator by Thomas Konings\n" +
    "\nUniWebView 3\n\n\n\n" +
    "\nSPECIAL THANKS\n" +
    "\n to SPOTIFY for making work pleasant";



