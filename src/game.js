GameManager.Game.prototype = {
    create: function () {
        currentScore = 0;
        let tempStep = 144;
        let verticalNumberOfBoxes = 8;

        gameProperties = {
            isGameOver: false,
            isNextLevel: false,
            gameOverSprite: null,
            end: false,
            step: tempStep,
            numberOfBoxes: {horizontal: 13, vertical: verticalNumberOfBoxes},
            startBoxesPosition: 3,
            startPosition: {x: 20, y: game.world.height - 89 - verticalNumberOfBoxes * tempStep},
            clawStartPosition: {x: -200, y: 0},
            smashCounter: 0,
            lastBox: 0,
            startNextBox: false,
            gamePaused: false,
            spawnBoxes: []

        };
        playerProperties = {
            EVENT: {
                WALK: 0,
                PUSH: 1,
                JUMP: 2,
                FALL: 3,
                IDLE: 4,
                TURN: 5,
                WALK_JUMP: 6,
                JUMP_OF: 7,
                JUMPOPUSH: 8,
            },
            currentState: 4,
            isPlayerFacingRight: true,
            animationTime: 400,
            animationFrameRate: 25,
            blockButtonTimeOffset: 10,
            isMoveBlocked: false,
            canUseSkill: true,
            position: {x: 6, y: gameProperties.numberOfBoxes.vertical - 1},
            offset: {x: 92, y: -304},
            isLongJump: false,
            swearGroup: null,
            swear: null,
            moveInProgress: false
        };

        currentLevel = levels[levelIndex];
        timeOfNextBox = currentLevel.timeOfNextBox;
        debugLog(gameProperties);
        debugLog(playerProperties);

        this.isDestroying = false;

        this.initEnvironment();
        this.initPlayer();
        this.initBoard();
        this.initScoreBoard();
        if (!Phaser.Device.desktop) {
            this.initMobileGraphics();
        }
        this.initSounds();

        // PREPARE ARRAY FOR RANDOM SPAWN BOXES - CHANCES DEPENDS ON COUNT PER BOX TYPE
        for (let i = 0; i < currentLevel.spawnBoxes.length; i++) {
            for (let j = 0; j < currentLevel.spawnBoxes[i].count; j++) {
                gameProperties.spawnBoxes.push(currentLevel.spawnBoxes[i].type)
            }
        }


        this.checkState();

        let graphics = game.add.graphics();
        graphics.beginFill(0x000000);
        graphics.drawRect(0, 0, game.world.width, game.world.height);
        graphics.endFill();
        graphics.alpha = 1;

        game.physics.startSystem(Phaser.Physics.ARCADE);
        playerProperties.isMoveBlocked = true;
        let levelText = game.add.text(game.world.width / 2, game.world.height / 2, (levelIndex === levels.length - 1) ? "FINAL LEVEL" : ("LEVEL " + (levelIndex + 1)), {
            fontSize: '60px',
            fill: "#ffffff"
        });

        this.initShelf();

        levelText.alpha = 0;
        levelText.anchor.set(0.5, 0.5);
        game.add
            .tween(levelText)
            .to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        game.add
            .tween(graphics)
            .to({alpha: 0.4}, 500, Phaser.Easing.Linear.None, true)
            .onComplete.add(() => {
            setTimeout(() => {
                game.add
                    .tween(levelText)
                    .to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
                game.add
                    .tween(graphics)
                    .to({alpha: 0}, 500, Phaser.Easing.Linear.None, true)
                    .onComplete.add(() => {
                    levelText.destroy();
                    playerProperties.isMoveBlocked = false;

                    setTimeout(() => {
                        game.onPause.add(() => {
                            gameProperties.gamePaused = true;
                        });
                        game.onFocus.add(() => {
                            gameProperties.gamePaused = false;
                            if (gameProperties.startNextBox) {
                                this.startBoxes();
                            }
                        });

                        this.startBoxes();
                    }, 1000);
                });
            }, 3000);


        });


    },

    startBoxes: function () {
        if (gameProperties.isGameOver || gameProperties.isNextLevel) {
            return;
        }
        gameProperties.startNextBox = false;
        this.dropBox();
        debugLog("Current drop time: " + timeOfNextBox);
        setTimeout(() => {
            gameProperties.startNextBox = true;
            if (!gameProperties.gamePaused) {
                if (timeOfNextBox > currentLevel.minimumTime) {
                    timeOfNextBox -= currentLevel.nextBoxOffsetTime;
                }
                if (timeOfNextBox < currentLevel.minimumTime) {
                    timeOfNextBox = currentLevel.minimumTime;
                }
                this.startBoxes();
            }
        }, timeOfNextBox);
    },
    addToScore: function (value) {
        if (gameProperties.isGameOver || gameProperties.isNextLevel) {
            return;
        }
        currentScore += value;
        PlaySound(pointsSound);
        this.updateScore();
    },
    movePlayer: function (direction) {
        if (gameProperties.isGameOver || (gameProperties.isNextLevel && direction !== Phaser.Keyboard.DOWN)) {
            return;
        }
        if (playerProperties.moveInProgress) {
            return;
        }
        playerProperties.moveInProgress = true;
        let changingFacing = false;
        playerProperties.isMoveBlocked = true;
        let nextX = playerProperties.position.x;
        let nextY = playerProperties.position.y;
        switch (direction) {
            case Phaser.Keyboard.LEFT: {
                playerProperties.currentState = playerProperties.EVENT.WALK;
                if (playerProperties.isPlayerFacingRight === true) {
                    changingFacing = true;
                    playerProperties.currentState = playerProperties.EVENT.TURN;
                }
                nextX -= 1;
                break;
            }
            case Phaser.Keyboard.RIGHT: {
                playerProperties.currentState = playerProperties.EVENT.WALK;
                if (playerProperties.isPlayerFacingRight === false) {
                    changingFacing = true;
                    playerProperties.currentState = playerProperties.EVENT.TURN;
                }
                nextX += 1;
                break;
            }
            case Phaser.Keyboard.UP: {
                playerProperties.currentState = playerProperties.EVENT.JUMP;
                if (playerProperties.isPlayerFacingRight === true) {
                    nextX += 1;
                    nextY -= 1;
                } else if (playerProperties.isPlayerFacingRight === false) {
                    nextX -= 1;
                    nextY -= 1;
                }
                break;
            }
            case Phaser.Keyboard.DOWN: {
                for (let i = playerProperties.position.y + 1; i < gameProperties.numberOfBoxes.vertical; i++) {
                    if (playBoard[nextX][i].SYMBOL === SYMBOL.EMPTY) {
                        playerProperties.currentState = playerProperties.EVENT.FALL;
                        nextY += 1;
                    }
                }
                break;
            }
            case "SPRINGS": {
                playerProperties.currentState = playerProperties.EVENT.JUMP;
                if (playerProperties.isPlayerFacingRight === true) {
                    nextX += 1;
                    nextY -= 3;
                } else if (playerProperties.isPlayerFacingRight === false) {
                    nextX -= 1;
                    nextY -= 3;
                }
                if (nextY < 2) {
                    nextY = 2;
                }
                break;
            }
        }
        if (playerProperties.currentState === playerProperties.EVENT.WALK) {
            if (nextX >= 0 && nextX < gameProperties.numberOfBoxes.horizontal && nextY + 1 < gameProperties.numberOfBoxes.vertical) {
                if (playBoard[nextX][nextY + 1].SYMBOL === SYMBOL.EMPTY) {
                    playerProperties.currentState = playerProperties.EVENT.JUMP_OF;
                    for (let i = playerProperties.position.y + 1; i < gameProperties.numberOfBoxes.vertical; i++) {
                        if (playBoard[nextX][i].SYMBOL === SYMBOL.EMPTY) {
                            nextY += 1;
                        }
                    }
                }
            }
        }


        if (changingFacing === true) {
            playerProperties.isPlayerFacingRight = !playerProperties.isPlayerFacingRight;
            if (playerProperties.isPlayerFacingRight == true) {
                player.scale.set(1, 1);
            } else {
                player.scale.set(-1, 1);
            }
            this.animationHandler();
            setTimeout(() => {
                playerProperties.moveInProgress = false;
                playerProperties.isMoveBlocked = false;
                this.checkState();
            }, playerProperties.animationTime + playerProperties.blockButtonTimeOffset);
        } else {
            if ((nextX !== playerProperties.position.x || nextY !== playerProperties.position.y) && this.isLegal(nextX, nextY, direction)) {
                if (playerProperties.currentState == playerProperties.EVENT.JUMP && direction != "SPRINGS") {
                    if (playerProperties.isPlayerFacingRight === true) {
                        if (playBoard[nextX][nextY + 1].SYMBOL === SYMBOL.EMPTY) {
                            nextX += 1;
                        }
                    } else {
                        if (playBoard[nextX][nextY + 1].SYMBOL === SYMBOL.EMPTY) {
                            nextX -= 1;
                        }
                    }
                    playerProperties.isLongJump = true;
                    if (!this.isLegal(nextX, nextY)) {
                        if (playerProperties.isPlayerFacingRight === true) {
                            nextX -= 1;
                        } else {
                            nextX += 1;
                        }
                    }
                }

                let currentTime = playerProperties.animationTime;
                if (playerProperties.isLongJump == true) {
                    currentTime = playerProperties.animationTime * 1.4;
                }
                this.animationHandler();
                this.exchangePlaces(nextX, nextY, {x: playerProperties.position.x, y: playerProperties.position.y});
                this.exchangePlaces(nextX, nextY - 1, {
                    x: playerProperties.position.x,
                    y: playerProperties.position.y - 1,
                });
                playerProperties.position.x = nextX;
                playerProperties.position.y = nextY;
                this.updateTweenPlayerPosition(currentTime);
                setTimeout(() => {
                    playerProperties.isMoveBlocked = false;
                    playerProperties.moveInProgress = false;
                    playerProperties.isLongJump = false;
                    this.checkState();
                    if (nextY + 1 < gameProperties.numberOfBoxes.vertical) {
                        if (playBoard[nextX][nextY + 1].SYMBOL === SYMBOL.EMPTY) {
                            this.movePlayer(Phaser.Keyboard.DOWN);
                        }
                    }
                }, currentTime + playerProperties.blockButtonTimeOffset);
                return true;
            } else {
                if (playerProperties.currentState == playerProperties.EVENT.WALK) {
                    playerProperties.moveInProgress = false;
                    this.movePlayer(Phaser.Keyboard.UP);
                } else {
                    playerProperties.isMoveBlocked = false;
                    playerProperties.moveInProgress = false;
                    this.checkState();
                }
                return false;
            }
        }
    },
    animationHandler: function () {
        player.animations.stop();

        switch (playerProperties.currentState) {
            case playerProperties.EVENT.TURN: {
                player.animations.play("turn", playerProperties.animationFrameRate);
                break;
            }
            case playerProperties.EVENT.WALK: {
                PlaySound(walkSound);
                player.animations.play("walk", playerProperties.animationFrameRate);
                break;
            }
            case playerProperties.EVENT.JUMP: {
                PlaySound(jumpSound);
                player.animations.play("jump", 35);
                break;
            }
            case playerProperties.EVENT.FALL: {
                PlaySound(jumpOfSound);
                player.animations.play("fall", 40);
                break;
            }
            case playerProperties.EVENT.JUMP_OF: {
                PlaySound(jumpOfSound);
                player.animations.play("jumpOf", 45);
                break;
            }
            case playerProperties.EVENT.PUSH: {
                if (getRandomInt(0, 20) === 0) {
                    PlaySound(pushSound);
                } else {
                    PlaySound(walkSound);
                }
                if (!player.isPushing) {
                    player.animations.getAnimation("startPush").onComplete.addOnce(() => {
                        this.push(false);
                    });
                    player.animations.play("startPush", playerProperties.animationFrameRate);
                } else {
                    this.push(true);
                }
                break;
            }
            case playerProperties.EVENT.JUMPOPUSH: {
                PlaySound(pushSound);
                player.animations.play("jumpopush", playerProperties.animationFrameRate);
                break;
            }
        }
        if (playerProperties.currentState === playerProperties.EVENT.PUSH) {
            player.isPushing = true;
        } else {
            player.isPushing = false;
        }
    },
    push: function (isLong) {
        let pushAnim = "";
        if (isLong) {
            pushAnim = "longPush";
        } else {
            pushAnim = "push";
        }
        if (getRandomInt(0, 20) === 0) {
            pushAnim += "2";
            PlaySound(fartSound);
        }
        const endPush = () => {
            player.animations.play("endPush", playerProperties.animationFrameRate);
        };

        if (player.animations.getAnimation(pushAnim).onComplete.has(endPush, "push")) {
            player.animations.getAnimation(pushAnim).onComplete.remove(endPush, "push");
        }
        player.animations.getAnimation(pushAnim).onComplete.add(endPush, "push");
        player.animations.play(pushAnim, playerProperties.animationFrameRate);
    },
    mumbleSwear: function () {
        if (gameProperties.isNextLevel) {
            return;
        }
        if (playerProperties.swearGroup == null && !gameProperties.isGameOver) {
            if (getRandomInt(0, 30) === 0) {
                PlaySound(swearSound);
                playerProperties.swearGroup = game.add.group();
                playerProperties.swear = game.add.sprite(0, 0, "textures_01", "swear");
                playerProperties.swearGroup.add(playerProperties.swear);
                playerProperties.swear.anchor.set(1, 1);
                playerProperties.swear.scale.set(1, 1);
                game.add
                    .tween(playerProperties.swear.scale)
                    .to(
                        {
                            x: 1,
                            y: 1,
                        },
                        300,
                        Phaser.Easing.Back.Out,
                        true
                    )
                    .onComplete.add(() => {
                    game.add
                        .tween(playerProperties.swear.scale)
                        .to(
                            {
                                x: 0.6,
                                y: 0.6,
                            },
                            300,
                            Phaser.Easing.Back.Out,
                            true
                        )
                        .onComplete.add(() => {
                        game.add
                            .tween(playerProperties.swear.scale)
                            .to(
                                {
                                    x: 1.3,
                                    y: 1.3,
                                },
                                300,
                                Phaser.Easing.Back.Out,
                                true
                            )
                            .onComplete.add(() => {
                            game.add
                                .tween(playerProperties.swear.scale)
                                .to(
                                    {
                                        x: 0,
                                        y: 0,
                                    },
                                    300,
                                    Phaser.Easing.Linear.None,
                                    true
                                )
                                .onComplete.add(() => {
                                playerProperties.swear.destroy();
                                playerProperties.swearGroup.destroy();
                                playerProperties.swear = null;
                                playerProperties.swearGroup = null;
                            });
                        });
                    });
                });
                game.add
                    .tween(playerProperties.swear)
                    .to({angle: getRandomInt(10, 30)}, 250, Phaser.Easing.Back.Out, true)
                    .onComplete.add(() => {
                    game.add
                        .tween(playerProperties.swear)
                        .to({angle: -getRandomInt(10, 30)}, 250, Phaser.Easing.Back.Out, true)
                        .onComplete.add(() => {
                        game.add
                            .tween(playerProperties.swear)
                            .to({angle: getRandomInt(10, 30)}, 250, Phaser.Easing.Back.Out, true)
                            .onComplete.add(() => {
                            game.add
                                .tween(playerProperties.swear)
                                .to({angle: -getRandomInt(10, 30)}, 250, Phaser.Easing.Back.Out, true)
                                .onComplete.add(() => {
                                game.add
                                    .tween(playerProperties.swear)
                                    .to({angle: 0}, 200, Phaser.Easing.Back.Out, true)
                                    .onComplete.add(() => {
                                });
                            });
                        });
                    });
                });
            }
        }
    },
    updateTweenPlayerPosition: function (diffrentTime) {
        let time = (diffrentTime) ? diffrentTime : playerProperties.animationTime;
        let valueX = playerProperties.position.x * gameProperties.step + playerProperties.offset.x;
        let valueY = playerProperties.position.y * gameProperties.step + playerProperties.offset.y;
        game.add.tween(player.position).to(
            {
                x: valueX,
            },
            time,
            Phaser.Easing.Linear.None,
            true
        );
        let ease = Phaser.Easing.Back.Out;
        if (playerProperties.currentState === playerProperties.EVENT.FALL) {
            ease = Phaser.Easing.Sinusoidal.In;
        } else if (playerProperties.currentState === playerProperties.EVENT.JUMP_OF) {
            ease = Phaser.Easing.Back.In;
        }
        game.add.tween(player.position).to(
            {
                y: valueY,
            },
            time,
            ease,
            true
        );
    },

    updatePlayerPosition: function () {
        player.position.set(playerProperties.position.x * gameProperties.step + playerProperties.offset.x, playerProperties.position.y * gameProperties.step + playerProperties.offset.y);
    },
    isLegal: function (x, y, direction) {
        if (x < 0 || x >= gameProperties.numberOfBoxes.horizontal || y < 1 || y - 1 < 1 || y >= gameProperties.numberOfBoxes.vertical) {
            return false;
        }


        if (this.isBoxType(x, y)) {
            if (y - 2 < 0) {
                if (this.isBoxType(x, y - 2)) {
                    return false;
                }
            }

            let facing = (playerProperties.isPlayerFacingRight) ? 1 : -1;
            let endComparing = (playerProperties.isPlayerFacingRight) ? gameProperties.numberOfBoxes.horizontal : 0;
            let keyboardDirection = (playerProperties.isPlayerFacingRight) ? Phaser.Keyboard.RIGHT : Phaser.Keyboard.LEFT;

            if (playerProperties.isPlayerFacingRight) {
                if (x + facing >= endComparing) {
                    return false;
                }
            } else {
                if (x + facing < endComparing) {
                    return false;
                }
            }

            if (this.isBoxType(x + facing, y)) {
                return false;
            }
            if (this.isBoxType(x, y - 1)) {
                return false;
            }

            if (y + 1 < gameProperties.numberOfBoxes.vertical) {
                if (!this.isBoxType(x, y + 1)) {
                    return false;
                }
            }
            if (playerProperties.isLongJump === true) {
                return false;
            }
            if (this.moveBox(keyboardDirection, {x: x, y: y})) {
                if (playerProperties.currentState === playerProperties.EVENT.JUMP) {
                    playerProperties.currentState = playerProperties.EVENT.JUMPOPUSH;
                } else {
                    playerProperties.currentState = playerProperties.EVENT.PUSH;
                }
                return true;
            } else {
                return false;
            }
        }
        return !!this.checkFallingBoxLegal(x, direction);
    },
    canBeSpawn(x, y, boxType) {
        if (playBoard[x][y].SYMBOL === SYMBOL.BOX) {
            return boxType === SYMBOL.EXPLOSION;

        } else if (playBoard[x][y].SYMBOL === SYMBOL.BRICK) {
            return boxType === SYMBOL.EXPLOSION;

        } else if (playBoard[x][y].SYMBOL === SYMBOL.PLAYER) {
            return false
        }
        return true
    },
    isBoxType(x, y) {
        if (playBoard[x][y].SYMBOL === SYMBOL.BOX) {
            return true
        } else if (playBoard[x][y].SYMBOL === SYMBOL.BRICK) {
            return true
        } else if (playBoard[x][y].SYMBOL === SYMBOL.EXPLOSION) {
            return true
        }
        return false

    },
    checkFallingBoxLegal: function (x, direction) {
        if (direction == "SPRINGS") {
            return true;
        }
        return !this.isBoxType(x, playerProperties.position.y - 1) && !this.isBoxType(x, playerProperties.position.y - 2)
    },
    moveBox: function (direction, coordinates) {
        if (playBoard[coordinates.x][coordinates.y].SYMBOL === SYMBOL.BRICK) {
            return false
        }
        let nextX = coordinates.x;
        let nextY = coordinates.y;
        switch (direction) {
            case Phaser.Keyboard.LEFT: {
                nextX -= 1;
                break;
            }
            case Phaser.Keyboard.RIGHT: {
                nextX += 1;
                break;
            }
        }
        if (this.isLegalBox(nextX, nextY)) {
            this.moveBoxSprite(coordinates, {x: nextX, y: nextY}, playerProperties.animationTime, null);
            this.exchangePlaces(nextX, nextY, coordinates);
            if (nextY + 1 < gameProperties.numberOfBoxes.vertical) {
                if (playBoard[nextX][nextY + 1].SYMBOL === SYMBOL.EMPTY) {

                    setTimeout(() => {
                        this.startFalling({x: nextX, y: nextY}, this.getSprite(coordinates));
                    }, playerProperties.animationTime);
                }
            }
            return true;
        } else {
            return false;
        }
    },
    isLegalBox: function (x, y) {
        if (x < 0 || x >= gameProperties.numberOfBoxes.horizontal || y < 0 || y >= gameProperties.numberOfBoxes.vertical) {
            return false;
        }
        return !this.isBoxType(x, y);

    },
    getSprite: function (coordinates) {
        return playBoard[coordinates.x][coordinates.y].sprite
    },
    moveBoxSprite: function (coordinates, coordinatesTarget, time, callback) {
        if (gameProperties.isGameOver) {
            return;
        }
        if (time <= 0) {
            playBoard[coordinates.x][coordinates.y].sprite.position.set(
                gameProperties.startPosition.x + coordinatesTarget.x * gameProperties.step,
                gameProperties.startPosition.y + coordinatesTarget.y * gameProperties.step
            );
        } else {
            const sprite = playBoard[coordinates.x][coordinates.y].sprite;
            if (sprite) {
                sprite.checkBox = false;
            }
            game.add
                .tween(playBoard[coordinates.x][coordinates.y].sprite)
                .to(
                    {
                        x: gameProperties.startPosition.x + coordinatesTarget.x * gameProperties.step,
                        y: gameProperties.startPosition.y + coordinatesTarget.y * gameProperties.step,
                    },
                    time,
                    Phaser.Easing.Linear.None,
                    true
                )
                .onComplete.add(() => {
                if (sprite) {
                    sprite.checkBox = true;
                }
                sprite.position.set(
                    gameProperties.startPosition.x + coordinatesTarget.x * gameProperties.step,
                    gameProperties.startPosition.y + coordinatesTarget.y * gameProperties.step
                );
                if (callback) {
                    callback();
                }
            }, this);
        }
    },
    startFalling: function (coordinates, sprite) {
        if (gameProperties.isGameOver) {
            return;
        }
        let currentCoordinates = coordinates;

        let nextY = currentCoordinates.y + 1;

        if (this.fallingLegal(currentCoordinates.x, currentCoordinates.y, nextY, sprite)) {
            this.moveBoxSprite(
                currentCoordinates,
                {
                    x: currentCoordinates.x,
                    y: nextY,
                },
                playerProperties.animationTime / 4,
            );
            setTimeout(() => {
                if (nextY + 1 < gameProperties.numberOfBoxes.vertical) {
                    if (!this.isBoxType(currentCoordinates.x, nextY + 1)) {
                        this.startFalling({x: currentCoordinates.x, y: nextY}, sprite);
                    } else {
                        if (sprite && sprite.isFalling === true) {
                            this.boxOnGround(sprite, currentCoordinates.x, nextY);
                        }
                    }
                } else {
                    if (sprite && sprite.isFalling === true) {
                        this.boxOnGround(sprite, currentCoordinates.x, nextY);
                    }
                }
                this.checkState();
            }, playerProperties.animationTime / 4);
            this.exchangePlaces(currentCoordinates.x, nextY, {x: currentCoordinates.x, y: currentCoordinates.y});
        } else {
            if (sprite && sprite.isFalling === true) {
                this.boxOnGround(sprite, currentCoordinates.x, currentCoordinates.y);
            }
        }
    },
    boxOnGround: function (sprite, x, y) {
        sprite.isFalling = false;
        if (playBoard[x][y].SYMBOL === SYMBOL.EXPLOSION) {
            this.explodeBox(x, y);
        }
        if (sprite) {
            this.addParticleBoxDown(
                {
                    x: sprite.x + sprite.width / 2,
                    y: sprite.y + sprite.height + 24,
                },
                600
            );
        }
    },
    fallingLegal: function (x, cy, y, sprite) {

        if (y < 0 || y >= gameProperties.numberOfBoxes.vertical) {
            return false;
        }
        if (this.isBoxType(x, y)) {
            return false;
        }
        if (playBoard[x][y].SYMBOL === SYMBOL.PLAYER) {
            if (playBoard[x][cy].SYMBOL === SYMBOL.EXPLOSION) {
                this.explodeBox(x, cy);
                return false;
            } else {
                if (!this.umbrella()) {
                    life.value--;
                    this.loseLifeSprite();
                    debugLog("Life left: " + life.value);
                }
                if (life.value == 0) {
                    gameProperties.gameOverSprite = sprite;
                    gameProperties.isGameOver = true;
                    return false;
                } else {
                    playBoard[x][cy].SYMBOL = SYMBOL.EMPTY;
                    this.lifeLostSmash(sprite);
                    playBoard[x][cy].sprite = null;
                    return false;
                }

            }
        }
        return true;
    },
    explodeBox: function (x, y) {

        //EXPLOSION ANIMATION
        let boardPlace = playBoard[x][y];
        let explosion = game.add.sprite(
            boardPlace.sprite.x + boardPlace.sprite.width / 2,
            boardPlace.sprite.y + boardPlace.sprite.height / 2,
            "textures_02",
            "explosion_01"
        );
        explosion.anchor.set(0.5, 0.5);
        explosion.animations.add("explode", ["explosion_01", "explosion_02", "explosion_03", "explosion_04", "explosion_05", "explosion_06", "explosion_07", "explosion_08", "explosion_09", "explosion_10", "explosion_11", "explosion_12"]);
        explosion.animations.getAnimation("explode").onComplete.add(() => {
            explosion.destroy();
        });
        PlaySound(explosionSound);
        explosion.animations.play("explode", 18, false, false)

        boardPlace.SYMBOL = SYMBOL.EMPTY;
        boardPlace.sprite.destroy();
        boardPlace.sprite = null;

        //EXPLOSION BOARD CHECK
        let canXLess = false, canXMore = false, canYLess = false, canYMore = false, playerHurt = false;
        if (x > 0) {
            canXLess = true
        }
        if (x < gameProperties.numberOfBoxes.horizontal - 1) {
            canXMore = true
        }
        if (y > 0) {
            canYLess = true
        }
        if (y < gameProperties.numberOfBoxes.vertical - 1) {
            canYMore = true
        }
        if (canXLess) {
            playerHurt = this.checkExplode(x - 1, y, playerHurt);
            if (canYLess) {
                playerHurt = this.checkExplode(x - 1, y - 1, playerHurt);
            }
            if (canYMore) {
                playerHurt = this.checkExplode(x - 1, y + 1, playerHurt);
            }
        }
        if (canXMore) {
            playerHurt = this.checkExplode(x + 1, y, playerHurt);
            if (canYLess) {
                playerHurt = this.checkExplode(x + 1, y - 1, playerHurt);
            }
            if (canYMore) {
                playerHurt = this.checkExplode(x + 1, y + 1, playerHurt);
            }
        }
        if (canYLess) {
            playerHurt = this.checkExplode(x, y - 1, playerHurt);
        }
        if (canYMore) {
            playerHurt = this.checkExplode(x, y + 1, playerHurt);
        }

        //PLAYER LOST LIFE
        if (playerHurt) {
            PlaySound(hitSound);
            life.value--;
            this.loseLifeSprite();
            debugLog("Life left: " + life.value);
            if (life.value === 0) {
                gameProperties.gameOverSprite = null;
                gameProperties.isGameOver = true;
            }
        }
        this.movePlayer(Phaser.Keyboard.DOWN)
    },
    checkExplode: function (x, y, playerHurt) {
        let placeCheck = playBoard[x][y];
        if (placeCheck.SYMBOL === SYMBOL.PLAYER) {
            return true;
        }
        if (placeCheck.SYMBOL !== SYMBOL.EMPTY) {
            this.addParticleBox(
                {
                    x: placeCheck.sprite.x + placeCheck.sprite.width / 2,
                    y: placeCheck.sprite.y + placeCheck.sprite.height / 2,
                },
                5000
            );
            placeCheck.SYMBOL = SYMBOL.EMPTY;
            placeCheck.sprite.destroy();
            placeCheck.sprite = null;
        }
        return playerHurt;
    },
    lifeLostSmash: function (sprite) {
        PlaySound(hitSound);
        game.add
            .tween(sprite)
            .to({y: "-10"}, 100, Phaser.Easing.Linear.None, true)
            .onComplete.add(() => {
            gameProperties.smashCounter = 0;
            this.addParticleBox(
                {
                    x: sprite.x + sprite.width / 2,
                    y: sprite.y + sprite.height / 2,
                },
                5000
            );
            sprite.destroy();
        });
    },
    checkBoxes: function () {
        if (gameProperties.isGameOver || gameProperties.isNextLevel) {
            return;
        }
        for (let i = gameProperties.numberOfBoxes.horizontal - 1; i >= 0; i--) {
            for (let j = gameProperties.numberOfBoxes.vertical - 1; j >= 0; j--) {
                if (this.isBoxType(i, j) && playBoard[i][j].sprite && playBoard[i][j].sprite.isFalling == false && playBoard[i][j].sprite.checkBox == true) {
                    this.startFalling({x: i, y: j}, playBoard[i][j].sprite);
                }
            }
        }
    },
    exchangePlaces: function (nextX, nextY, coordinates) {
        let temp = playBoard[coordinates.x][coordinates.y];
        playBoard[coordinates.x][coordinates.y] = playBoard[nextX][nextY];
        playBoard[nextX][nextY] = temp;
    },
    checkState: function () {
        this.drawTable();
        this.logInfo();
        this.checkBoxes();
        this.checkBottomLine();
        debugLog("SCORE IN THIS ROUND: " + currentScore + " SCORE IN PAST ROUNDS: " + currentTotalScore);
        debugLog(currentLevel);
        this.updateScore();
        this.updatePlayerPosition();


        if (gameProperties.isNextLevel) {
            return;
        }
        if (currentScore >= currentLevel.levelCompleteScore && levelIndex !== levels.length - 1) {
            gameProperties.isNextLevel = true;
            this.nextLevel();
            return;
        }
        playerProperties.currentState = playerProperties.EVENT.IDLE;

        if (gameProperties.isGameOver === true) {
            if (gameProperties.end == false) {
                gameProperties.end = true;

                debugLog("GAME OVER!");
                player.animations.stop();

                player.animations.play("halfTurn", 23);
                player.scale.x *= -1;
                let angleWings = game.add.sprite(player.x, 0, "textures", "wings");
                let playerDeath = game.add.sprite(
                    gameProperties.startPosition.x + 75,
                    gameProperties.startPosition.y + (gameProperties.numberOfBoxes.vertical - 2) * gameProperties.step,
                    "playerGameOver",
                    "player_death_01"
                );
                playerDeath.animations.add("playerGameOver", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11]);
                playerDeath.play("playerGameOver", 11);
                playerDeath.anchor.set(player.anchor.x, player.anchor.y);
                playerDeath.scale.set(player.scale.x, player.scale.y);
                playerDeath.position.set(player.x, player.y);
                let playerDeath2 = game.add.sprite(
                    gameProperties.startPosition.x + 75,
                    gameProperties.startPosition.y + (gameProperties.numberOfBoxes.vertical - 2) * gameProperties.step,
                    "playerGameOver",
                    "player_death_12"
                );
                playerDeath2.anchor.set(player.anchor.x, player.anchor.y);
                playerDeath2.scale.set(player.scale.x, player.scale.y);
                playerDeath2.position.set(player.x, player.y);
                playerDeath2.filters = [game.add.filter("Glow")];
                playerDeath2.alpha = 0;

                angleWings.anchor.set(playerDeath.anchor.x, 1);
                player.destroy();
                PlaySound(hitSound);


                let boxSprite = gameProperties.gameOverSprite;
                if (boxSprite == null) {
                    if (playBoard[playerProperties.position.x][playerProperties.position.y - 2].sprite != null) {
                        boxSprite = playBoard[playerProperties.position.x][playerProperties.position.y - 2].sprite;
                    } else if (playBoard[playerProperties.position.x][playerProperties.position.y - 2].sprite != null) {
                        boxSprite = playBoard[playerProperties.position.x][playerProperties.position.y - 1].sprite;
                    } else {
                        this.gameOver(playerDeath2, angleWings, playerDeath);
                    }
                }
                game.add
                    .tween(boxSprite)
                    .to({y: "+36"}, 220, Phaser.Easing.Sinusoidal.Out, true)
                    .onComplete.add(() => {
                    game.add
                        .tween(boxSprite)
                        .to({y: "-10"}, 100, Phaser.Easing.Linear.None, true, 580)
                        .onComplete.add(() => {
                        gameProperties.smashCounter = 0;
                        this.addParticleBox(
                            {
                                x: boxSprite.x + boxSprite.width / 2,
                                y: boxSprite.y + boxSprite.height / 2,
                            },
                            5000
                        );
                        boxSprite.destroy();

                        this.gameOver(playerDeath2, angleWings, playerDeath);
                    });
                });
            }
        }
    },
    gameOver: function (playerDeath2, angleWings, playerDeath) {
        PlaySound(angelsSound);
        game.add.tween(playerDeath2).to({alpha: 0.7}, 4000, Phaser.Easing.Linear.None, true, 2000);
        stopBackgroundMusic();
        game.add
            .tween(angleWings)
            .to({y: playerDeath.y + 190}, 2500, Phaser.Easing.Back.Out, true)
            .onComplete.add(() => {
            game.add.tween(playerDeath).to({y: "-1000"}, 3500, Phaser.Easing.Cubic.In, true, 100);
            game.add.tween(playerDeath2).to({y: "-1000"}, 3500, Phaser.Easing.Cubic.In, true, 100);
            game.add
                .tween(angleWings)
                .to({y: "-1000"}, 3500, Phaser.Easing.Cubic.In, true, 100)
                .onComplete.add(() => {
                setTimeout(() => {
                    console.clear();
                    let graphics = game.add.graphics();
                    graphics.beginFill(0x000000);
                    graphics.drawRect(0, 0, game.world.width, game.world.height);
                    graphics.endFill();
                    graphics.alpha = 0;
                    game.add
                        .tween(graphics)
                        .to({alpha: 1}, 500, Phaser.Easing.Linear.None, true)
                        .onComplete.add(() => {
                        for (let i = 0; i < gameProperties.numberOfBoxes.horizontal; i++) {
                            for (let j = 0; j < gameProperties.numberOfBoxes.vertical; j++) {
                                if (playBoard[i][j].sprite != null) {
                                    if (playBoard[i][j].sprite.destroy) {
                                        playBoard[i][j].sprite.destroy();
                                    }
                                    playBoard[i][j] = null;
                                }
                            }
                        }
                        currentTotalScore += currentScore;
                        game.state.start("Score");
                    });
                }, 1000);
            });
        });
    },
    nextLevel: function () {
        this.updateScore();
        overlay = game.add.graphics();
        overlay.beginFill(0x000000, 1);
        overlay.drawRect(0, 0, game.world.width, game.world.height);
        overlay.endFill();
        overlay.alpha = 0;
        game.world.bringToTop(overlay);
        game.world.bringToTop(player);

        game.add.tween(overlay).to({alpha: 0.2}, 1500, Phaser.Easing.Linear.None, true);

        let claw_group = game.add.group();
        let chain = game.add.sprite(109, 32, "textures_02", "chain");
        let clawBase = game.add.sprite(68, 0, "textures", "clawBase");
        let clawLeft = game.add.sprite(96, 66, "textures", "clawLeft");
        let clawRight = game.add.sprite(120, 66, "textures", "clawRight");


        clawBase.scale.set(0.5, 0.5);
        clawLeft.scale.set(0.5, 0.5);
        clawRight.scale.set(0.5, 0.5);
        chain.scale.set(0.5, 0.5);

        clawLeft.anchor.set(0.98, 0.02);
        clawRight.anchor.set(0.02, 0.02);
        chain.anchor.set(0.5, 1);
        claw_group.add(chain);
        claw_group.add(clawBase);
        claw_group.add(clawLeft);
        claw_group.add(clawRight);
        claw_group.position.set(gameProperties.clawStartPosition.x, gameProperties.clawStartPosition.y);
        claw_group.pivot.x = claw_group.width / 2;
        claw_group.angle = 10;
        PlaySound(clawSound);
        game.world.bringToTop(claw_group);


        game.add
            .tween(claw_group)
            .to({x: (playerProperties.position.x * gameProperties.step) + (player.width / 2 * player.scale.x) + 6}, 1600, Phaser.Easing.Linear.None, true)
            .onComplete.add(() => {
            player.scale.x *= -1;
            player.animations.play("halfTurn", playerProperties.animationFrameRate);
            game.add
                .tween(claw_group)
                .to({angle: -10}, 300, Phaser.Easing.Back.Out, true)
                .onComplete.add(() => {
                game.add
                    .tween(claw_group)
                    .to({angle: 0}, 1000, Phaser.Easing.Elastic.Out, true)
                    .onComplete.add(() => {
                    PlaySound(chainSound);

                    game.add
                        .tween(claw_group)
                        .to({y: ((playerProperties.position.y - 1) * gameProperties.step) - player.height + 20}, 1500, Phaser.Easing.Back.Out, true)
                        .onComplete.add(() => {
                        player.scale.x *= -1;
                        player.animations.play("next_level", playerProperties.animationFrameRate);
                        game.world.bringToTop(player);
                        game.add.tween(clawLeft).to({angle: -14}, 200, Phaser.Easing.Linear.None, true, 50);
                        game.add.tween(clawRight).to({angle: 14}, 200, Phaser.Easing.Linear.None, true, 50);
                        game.add.tween(claw_group).to({y: claw_group.y - 20}, 200, Phaser.Easing.Linear.None, true, 150);
                        game.world.bringToTop(overlay);
                        game.world.bringToTop(claw_group);
                        game.world.bringToTop(player);


                        let backPosition = Math.abs(gameProperties.clawStartPosition.y - claw_group.position.y) - 20;
                        let outUpTime = 800;
                        let outRightTime = 1900;
                        let upDelay = 700;

                        game.add.tween(claw_group).to({angle: 4}, 200, Phaser.Easing.Linear.None, true, outUpTime + upDelay + 200);
                        game.add.tween(claw_group).to({y: gameProperties.clawStartPosition.y}, outUpTime, Phaser.Easing.Linear.None, true, upDelay).onStart.add(() => {
                            PlaySound(chainSound);
                        });
                        game.add
                            .tween(claw_group)
                            .to({x: game.world.width + 200}, outRightTime, Phaser.Easing.Linear.None, true, outUpTime + upDelay + 200)
                            .onComplete.add(() => {
                            claw_group.destroy();
                            if (++levelIndex > levels.length - 1) {
                                currentTotalScore += currentScore;
                                game.clear();

                                game.state.start("Score");
                            } else {
                                currentTotalScore += currentScore;
                                game.state.start("Game");
                            }
                        });
                        game.add.tween(player).to({angle: 4}, 200, Phaser.Easing.Linear.None, true, outUpTime + upDelay + 200);
                        game.add.tween(player).to({y: player.y - backPosition}, outUpTime, Phaser.Easing.Linear.None, true, upDelay);
                        game.add
                            .tween(player)
                            .to({x: game.world.width + 200}, outRightTime, Phaser.Easing.Linear.None, true, outUpTime + upDelay + 200)
                            .onStart.add(() => {
                            PlaySound(clawSound);
                            game.world.bringToTop(overlay);
                            game.world.bringToTop(claw_group);
                            game.world.bringToTop(player);

                        });


                    });
                });
            });
        });
    },
    addParticleBoxDown: function (coordinates, time) {
        PlaySound(boxDropSound);
        let emitter = game.add.emitter(coordinates.x, coordinates.y);

        emitter.makeParticles("textures_01", ["cloud_01", "cloud_02", "cloud_03", "cloud_04", "cloud_05"]);

        emitter.width = 100;
        emitter.setXSpeed(-100, 100);
        emitter.setYSpeed(-75, -140);
        emitter.gravity = 300;

        emitter.bringToTop = true;
        emitter.setScale(0, 0.8, 0, 0.8, time, Phaser.Easing.Linear.None, true);
        emitter.minRotation = -360;
        emitter.maxRotation = 360;
        emitter.setAlpha(1, 0, time);
        emitter.start(false, time, 25, 10);
        setTimeout(() => {
            if (emitter != null) {
                if (emitter.particles != null) {
                    emitter.destroy();
                }
            }
        }, time);
    },
    addParticleBox: function (coordinates, time) {
        if (gameProperties.smashCounter % 4 == 0) {
            PlaySound(boxSmashSound);
        }
        let emitter = game.add.emitter(coordinates.x, coordinates.y);

        emitter.makeParticles("textures_01", ["part_01", "part_02", "part_03", "part_04"]);

        emitter.setXSpeed(-100, 100);
        emitter.setYSpeed(-75, -140);

        emitter.bringToTop = true;
        emitter.setScale(-0.5, 0.5, 0.25, 0.25, 3000, Phaser.Easing.Sinusoidal.InOut, true);
        emitter.gravity = 300;

        emitter.start(true, time * 2, 250, 10);
        setTimeout(() => {
            if (emitter != null) {
                if (emitter.particles != null) {
                    emitter.destroy();
                }
            }
        }, time * 2);
    },
    checkBottomLine: function () {
        if (gameProperties.isGameOver || this.isDestroying) {
            return;
        }

        setTimeout(() => {
            if (gameProperties.isGameOver || this.isDestroying) {
                return;
            }
            let lineToDestroy = null, countBoxes;
            for (let k = gameProperties.numberOfBoxes.vertical - 1; k >= 0; k--) {
                countBoxes = 0;
                for (let j = 0; j < gameProperties.numberOfBoxes.horizontal; j++) {
                    if (playBoard[j][k].SYMBOL === SYMBOL.BOX) {
                        countBoxes++;
                    } else {
                        break;
                    }
                }
                if (countBoxes === gameProperties.numberOfBoxes.horizontal) {
                    lineToDestroy = k;
                    break;
                }
            }
            if (lineToDestroy == null || this.isDestroying) {
                return;
            }
            this.isDestroying = true;
            this.addToScore(currentLevel.scoreRow);
            this.getEquipment();
            gameProperties.smashCounter = 0;
            for (let j = 0; j < gameProperties.numberOfBoxes.horizontal; j++) {
                this.destroyBoxTimeOut(j, lineToDestroy);
            }
            setTimeout(() => {
                this.isDestroying = false;
                this.drawTable();
                for (let i = gameProperties.numberOfBoxes.horizontal - 1; i >= 0; i--) {
                    for (let j = gameProperties.numberOfBoxes.vertical - 1; j >= 0; j--) {
                        if (this.isBoxType(i, j)) {
                            this.startFalling({x: i, y: j}, playBoard[i][j].sprite);
                        } else if (playBoard[i][j].SYMBOL === SYMBOL.PLAYER) {
                            this.movePlayer(Phaser.Keyboard.DOWN);
                        }
                    }
                }
            }, gameProperties.numberOfBoxes.horizontal * 40);
        }, 1500);
    },

    destroyBoxTimeOut: function (index, line) {
        setTimeout(() => {
            this.destroyBox(index, line);
            for (let j = line; j >= 0; j--) {
                if (this.isBoxType(index, j)) {
                    this.startFalling({x: index, y: j}, playBoard[index][j].sprite);
                } else if (playBoard[index][j].SYMBOL === SYMBOL.PLAYER) {
                    this.movePlayer(Phaser.Keyboard.DOWN);
                }
            }
        }, 30 * index);
    },
    destroyBox: function (index, line) {
        let boxObject = playBoard[index][line];
        gameProperties.smashCounter += 1;

        if (boxObject.SYMBOL === SYMBOL.BRICK) {
            let emitter = game.add.emitter(0, 0);

            emitter.makeParticles("textures_01", ["cloud_01", "cloud_02", "cloud_03", "cloud_04", "cloud_05"]);
            emitter.width = boxObject.sprite.width;
            emitter.height = boxObject.sprite.height;
            emitter.setXSpeed(-100, 100);
            emitter.setYSpeed(-75, -140);
            emitter.gravity = 300;
            emitter.bringToTop = true;
            emitter.setScale(0.4, 0.6, 0.4, 0.6, 1000, Phaser.Easing.Linear.None, false);
            emitter.minRotation = -360;
            emitter.maxRotation = 360;
            emitter.setAlpha(1, 0, 1000);
            emitter.emitX = boxObject.sprite.x + boxObject.sprite.width / 2;
            emitter.emitY = boxObject.sprite.y + boxObject.sprite.height / 2;
            emitter.start(true, 2000, 1, 20);
            if (emitter != null) {
                setTimeout(() => {
                    emitter.destroy();
                }, 2000);
            }
        } else {
            this.addParticleBox(
                {
                    x: boxObject.sprite.x + boxObject.sprite.width / 2,
                    y: boxObject.sprite.y + boxObject.sprite.height / 2,
                },
                5000
            );
        }
        boxObject.SYMBOL = SYMBOL.EMPTY;
        boxObject.sprite.destroy();
        boxObject.sprite = null;
    },
    dropBox: function () {
        if (gameProperties.isGameOver || gameProperties.isNextLevel) {
            return;
        }
        let counter = 0;
        let clawColumn;
        let list = [];
        for (let i = 0; i < gameProperties.numberOfBoxes.horizontal; i++) {
            list.push(i);
        }
        list = shuffleArray(list);
        let boxType = this.getSpawnBoxType();

        for (let i = 0; i < list.length; i++) {
            if (list[i] === gameProperties.lastBox || !this.canBeSpawn(list[i], gameProperties.startBoxesPosition, boxType)) {
                if (++counter === list.length - 1) {
                    gameProperties.isGameOver = true;
                    this.checkState();
                    return;
                }
            } else {
                clawColumn = list[i];
                break;
            }

        }

        let claw = this.initClaws();
        claw.add(this.getBox(boxType));
        game.world.bringToTop(this.shelf);
        game.world.bringToTop(this.scoreGroup);


        if (!Phaser.Device.desktop) {
            game.world.bringToTop(mobileButtons);

        }
        claw.getChildAt(1).angle = 0;
        claw.getChildAt(2).angle = 0;

        gameProperties.lastBox = clawColumn;
        let clawOffsetX = 70.5;
        let clawOffsetY = -161.5;
        let distance = gameProperties.startPosition.x + clawColumn * gameProperties.step;
        let time = Math.abs(claw.x) + (clawOffsetX + distance) / currentLevel.clawSpeed;
        claw.angle = 10;

        game.add
            .tween(claw)
            .to({x: clawOffsetX + distance}, time, Phaser.Easing.Linear.None, true)
            .onComplete.add(() => {
            game.add
                .tween(claw)
                .to({angle: -10}, 300, Phaser.Easing.Back.Out, true)
                .onComplete.add(() => {
                game.add
                    .tween(claw)
                    .to({angle: 0}, 1000, Phaser.Easing.Elastic.Out, true)
                    .onComplete.add(() => {
                    setTimeout(() => {
                        time = (game.world.width - claw.x + 200) / currentLevel.clawSpeed;

                        if (this.canBeSpawn(clawColumn, gameProperties.startBoxesPosition, boxType)) {
                            this.addToScore(currentLevel.scoreForEveryDroppedBox);

                            let sprite = claw.children.pop();
                            this.mumbleSwear();
                            game.add.existing(sprite);
                            game.world.bringToTop(player);
                            game.world.bringToTop(this.shelf);
                            game.world.bringToTop(this.scoreGroup);

                            if (!Phaser.Device.desktop) {
                                game.world.bringToTop(mobileButtons);
                            }
                            sprite.position.set(distance, gameProperties.startPosition.y + clawOffsetY + gameProperties.step * gameProperties.startBoxesPosition);
                            if (gameProperties.isGameOver || gameProperties.isNextLevel) {
                                return;
                            }
                            if (sprite) {
                                sprite.isFalling = true;
                            }
                            game.add
                                .tween(sprite)
                                .to(
                                    {
                                        x: distance,
                                        y: gameProperties.startPosition.y + gameProperties.step * gameProperties.startBoxesPosition,
                                    },
                                    playerProperties.animationTime / 4,
                                    Phaser.Easing.Linear.None,
                                    true,
                                    150
                                )
                                .onComplete.add(() => {
                                if (this.canBeSpawn(clawColumn, gameProperties.startBoxesPosition, boxType) && (playBoard[clawColumn][gameProperties.startBoxesPosition].sprite == null || boxType === SYMBOL.EXPLOSION)) {
                                    if (playBoard[clawColumn][gameProperties.startBoxesPosition].sprite != null) {
                                        playBoard[clawColumn][gameProperties.startBoxesPosition].sprite.parent.removeChild(playBoard[clawColumn][gameProperties.startBoxesPosition].sprite);
                                        playBoard[clawColumn][gameProperties.startBoxesPosition].sprite.destroy();
                                    }
                                    playBoard[clawColumn][gameProperties.startBoxesPosition].SYMBOL = boxType;
                                    playBoard[clawColumn][gameProperties.startBoxesPosition].sprite = sprite;
                                    this.startFalling({
                                        x: clawColumn,
                                        y: gameProperties.startBoxesPosition
                                    }, this.getSprite({x: clawColumn, y: gameProperties.startBoxesPosition}));
                                }

                            }, this);

                            game.add.tween(claw.getChildAt(2)).to({angle: -getRandomInt(15, 25)}, 300, Phaser.Easing.Linear.None, true);

                            game.add
                                .tween(claw.getChildAt(1))
                                .to({angle: getRandomInt(15, 25)}, 300, Phaser.Easing.Linear.None, true)
                                .onComplete.add(() => {
                                if (gameProperties.isGameOver || gameProperties.isNextLevel) {
                                    return;
                                }
                                game.add.tween(claw.getChildAt(2)).to({angle: getRandomInt(15, 25)}, 200, Phaser.Easing.Linear.None, true, 150);
                                game.add.tween(claw.getChildAt(1)).to({angle: -getRandomInt(15, 25)}, 200, Phaser.Easing.Linear.None, true, 150);
                                game.add.tween(claw).to({angle: 10}, 300, Phaser.Easing.Linear.None, true, 300);
                                game.add
                                    .tween(claw)
                                    .to({x: game.world.width + 200}, time, Phaser.Easing.Linear.None, true, 500)
                                    .onComplete.add(() => {
                                    claw.destroy();
                                }, this);
                            }, this);
                        } else {
                            game.add
                                .tween(claw)
                                .to({x: game.world.width + 200}, time, Phaser.Easing.Linear.None, true, 500)
                                .onComplete.add(() => {
                                claw.destroy();
                            }, this);
                        }
                    }, 200);
                }, this);
            }, this);
        }, this);
    },
    getBox: function (type) {
        if (!type) {
            type = SYMBOL.BOX;
        }
        let box;
        switch (type) {
            case SYMBOL.BOX: {
                let boxData = currentLevel.assets.boxesSprites[getRandomInt(0, currentLevel.assets.boxesSprites.length - 1)];
                box = game.add.sprite(38, 110, boxData.atlas, boxData.name);
                box.checkBox = true;
                break;
            }
            case SYMBOL.BRICK: {
                box = game.add.sprite(38, 110, "textures_04", "box_bricks");
                box.checkBox = true;
                break;
            }
            case SYMBOL.EXPLOSION: {
                box = game.add.sprite(38, 110, "textures_04", "box_exploding");
                box.checkBox = true;
                break;
            }
        }
        return box;
    },
    getSpawnBoxType: function () {
        return gameProperties.spawnBoxes[getRandomInt(0, gameProperties.spawnBoxes.length - 1)];
    },
    logInfo: function () {
        let log = "Player x: " + playerProperties.position.x + " ,Player y: " + playerProperties.position.y + " ,PlayerFacing: ";
        log += +playerProperties.isPlayerFacingRight ? "Right" : "Left";
        debugLog(log);
    },
    drawTable: function () {
        let txt = "";
        let i, j;
        for (j = 0; j < gameProperties.numberOfBoxes.horizontal; j++) {
            txt += "+---";
            if (j > 9) {
                txt += "-";
            }
        }
        txt += "+ \n";
        for (j = 0; j < gameProperties.numberOfBoxes.horizontal; j++) {
            txt += "| " + j + " ";
        }
        txt += "| \n";
        for (j = 0; j < gameProperties.numberOfBoxes.horizontal + 1; j++) {
            txt += "+---";
            if (j > 9) {
                txt += "-";
            }
        }
        txt += "+ \n";
        for (i = 0; i < gameProperties.numberOfBoxes.vertical; i++) {
            txt += "| ";
            for (j = 0; j < gameProperties.numberOfBoxes.horizontal; j++) {
                if (playBoard[j][i].SYMBOL === SYMBOL.EMPTY) {
                    txt += " ";
                } else if (playBoard[j][i].SYMBOL === SYMBOL.BOX) {
                    txt += "T";
                } else if (playBoard[j][i].SYMBOL === SYMBOL.BRICK) {
                    txt += "B";
                } else if (playBoard[j][i].SYMBOL === SYMBOL.EXPLOSION) {
                    txt += "E";
                } else {
                    if (playerProperties.isPlayerFacingRight) {
                        txt += "\u25F7";
                    } else {
                        txt += "\u25F4";
                    }
                }
                txt += " | ";
                if (j > 8) {
                    txt += " ";
                }
            }
            txt += i + " | \n";

            for (j = 0; j < gameProperties.numberOfBoxes.horizontal + 1; j++) {
                txt += "+---";
                if (j > 9) {
                    txt += "-";
                }
            }
            txt += "+\n";
        }

        debugLog(txt);
    },
    update: function () {
        if (gameProperties.isGameOver || playerProperties.isMoveBlocked) {
            return;
        }
        if (Phaser.Device.desktop) {
            if (keyLeft.isDown) {
                this.movePlayer(Phaser.Keyboard.LEFT);
            } else if (keyRight.isDown) {
                this.movePlayer(Phaser.Keyboard.RIGHT);
            } else if (keyUp.isDown) {
                this.movePlayer(Phaser.Keyboard.UP);
            } else if (keyO.isDown) {
                if (!debug) {
                    return;
                }
                this.addToScore(2000);
            } else if (keyP.isDown) {
                if (!debug) {
                    return;
                }
                let random = getRandomInt(0, 4), count = 0;
                for (let i in equipment) {
                    if (count === random) {
                        equipment[i]++;
                        break;
                    }
                    count++;
                }
                this.updateShelf();
            } else if (keyL.isDown) {
                if (!debug) {
                    return;
                }
                let random = getRandomInt(0, 4), count = 0;
                for (let i in equipment) {
                    if (count === random) {
                        if (equipment[i] > 0) {
                            equipment[i]--;
                        }
                        break;
                    }
                    count++;
                }
                this.updateShelf();
            }
        } else {
            if (isLeftPressed) {
                this.movePlayer(Phaser.Keyboard.LEFT);
            } else if (isRightPressed) {
                this.movePlayer(Phaser.Keyboard.RIGHT);
            } else if (isJumpPressed) {
                this.movePlayer(Phaser.Keyboard.UP);
            }
        }
        if (playerProperties.swearGroup != null) {
            if (playerProperties.isPlayerFacingRight) {
                playerProperties.swearGroup.position.set(player.x + 50, player.y + 100);
                playerProperties.swearGroup.scale.set(-1, 1);
            } else {
                playerProperties.swearGroup.position.set(player.x - 50, player.y + 100);
                playerProperties.swearGroup.scale.set(1, 1);
            }
        }
    },
    initClaws: function () {
        let claw_group = game.add.group();
        let clawBase = game.add.sprite(68, 0, "textures", "clawBase");
        let clawLeft = game.add.sprite(0 + 96, 66, "textures", "clawLeft");
        let clawRight = game.add.sprite(118 + 2, 66, "textures", "clawRight");
        clawBase.scale.set(0.5, 0.5);
        clawLeft.scale.set(0.5, 0.5);
        clawRight.scale.set(0.5, 0.5);
        clawLeft.anchor.set(0.98, 0.02);
        clawRight.anchor.set(0.02, 0.02);

        claw_group.add(clawBase);
        claw_group.add(clawLeft);
        claw_group.add(clawRight);
        claw_group.position.set(gameProperties.clawStartPosition.x, gameProperties.clawStartPosition.y);
        claw_group.pivot.x = claw_group.width / 2;
        claw_group.angle = 10;
        return claw_group;
    },
    initEnvironment: function () {
        background = game.add.sprite(0, 0, currentLevel.assets.background);
        ground = game.add.sprite(0, game.world.height - 89, currentLevel.assets.ground.atlas, currentLevel.assets.ground.name);
        wallLeft = game.add.sprite(currentLevel.assets.leftWall.offset, 0, currentLevel.assets.leftWall.atlas, currentLevel.assets.leftWall.name);
        wallRight = game.add.sprite(game.world.width + currentLevel.assets.rightWall.offset, 0, currentLevel.assets.rightWall.atlas, currentLevel.assets.rightWall.name);
        soundOn = game.add.sprite(game.world.width - 160, game.world.height - 80, "textures", "soundOn");
        soundOff = game.add.sprite(game.world.width - 160, game.world.height - 80, "textures", "soundOff");

        light1 = game.add.sprite(game.world.centerX - 534, 76, "textures", "light");
        light2 = game.add.sprite(game.world.centerX + 14, 76, "textures", "light");
        light3 = game.add.sprite(game.world.centerX + 540, 74, "textures", "light");

        soundOn.scale.set(0.7, 0.7);
        soundOff.scale.set(0.7, 0.7);
        light1.anchor.set(0.5, 0);
        light2.anchor.set(0.5, 0);
        light3.anchor.set(0.5, 0);
        light1.alpha = 0.5;
        light2.alpha = 0.5;
        light3.alpha = 0.5;
    },
    initFloorBoxes: function (coordinates, type) {
        playBoard[coordinates.x][coordinates.y].SYMBOL = type;
        playBoard[coordinates.x][coordinates.y].sprite = this.getBox(type);
        playBoard[coordinates.x][coordinates.y].sprite.isFalling = false;
        this.moveBoxSprite({x: coordinates.x, y: coordinates.y}, {x: coordinates.x, y: coordinates.y}, 0, null);
    },
    initMobileGraphics: function () {
        mobileButtons = game.add.group();
        mobileButtons.alpha = 0.5;
        isLeftPressed = false;
        isRightPressed = false;
        isJumpPressed = false;

        leftButton = game.add.sprite(0, game.world.centerY, "textures_01", "ui_left");
        rightButton = game.add.sprite(game.world.width, game.world.centerY, "textures_01", "ui_right");
        jumpButton = game.add.sprite(game.world.centerX, game.world.height, "textures_01", "ui_jump");

        rightButton.scale.set(0.8, 0.8);
        leftButton.scale.set(0.8, 0.8);
        jumpButton.scale.set(0.55, 0.55);

        leftButtonArea = game.add.graphics();
        leftButtonArea.beginFill(0xfff, 0.001);
        leftButtonArea.drawRect(0, 0, game.world.centerX / 2.5, game.world.height);
        leftButtonArea.drawRect(0, 0, game.world.centerX, game.world.height / 2 + 100);
        leftButtonArea.endFill();

        rightButtonArea = game.add.graphics();
        rightButtonArea.beginFill(0xddd, 0.001);
        rightButtonArea.drawRect(game.world.width - game.world.centerX / 2.5, 0, game.world.centerX / 2.5, game.world.height);
        rightButtonArea.drawRect(game.world.centerX, 0, game.world.centerX, game.world.height / 2 + 100);
        rightButtonArea.endFill();

        jumpButtonArea = game.add.graphics();
        jumpButtonArea.beginFill(0x666, 0.001);
        jumpButtonArea.drawRect(game.world.centerX / 2.5, game.world.height / 2 + 100, game.world.width - (game.world.centerX / 2.5) * 2, game.world.height - (game.world.height / 2 + 100));
        jumpButtonArea.endFill();

        leftButton.anchor.set(0, 0.5);
        rightButton.anchor.set(1, 0.5);
        jumpButton.anchor.set(0.5, 1);

        leftButtonArea.inputEnabled = true;
        rightButtonArea.inputEnabled = true;
        jumpButtonArea.inputEnabled = true;

        leftButtonArea.events.onInputDown.add(() => {
            isLeftPressed = true;
        }, this);
        rightButtonArea.events.onInputDown.add(() => {
            isRightPressed = true;
        }, this);
        jumpButtonArea.events.onInputDown.add(() => {
            isJumpPressed = true;
        }, this);

        leftButtonArea.events.onInputUp.add(() => {
            isLeftPressed = false;
        }, this);
        rightButtonArea.events.onInputUp.add(() => {
            isRightPressed = false;
        }, this);
        jumpButtonArea.events.onInputUp.add(() => {
            isJumpPressed = false;
        }, this);
        mobileButtons.add(leftButton);
        mobileButtons.add(rightButton);
        mobileButtons.add(jumpButton);
    },
    initPlayer: function () {
        keyLeft = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        keyRight = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        keyUp = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        keyDown = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        keyA = game.input.keyboard.addKey(Phaser.Keyboard.A);
        keyS = game.input.keyboard.addKey(Phaser.Keyboard.S);
        keyD = game.input.keyboard.addKey(Phaser.Keyboard.D);
        keyP = game.input.keyboard.addKey(Phaser.Keyboard.P);
        keyL = game.input.keyboard.addKey(Phaser.Keyboard.L);
        keyO = game.input.keyboard.addKey(Phaser.Keyboard.O);
        player = game.add.sprite(
            gameProperties.startPosition.x + 75,
            gameProperties.startPosition.y + (gameProperties.numberOfBoxes.vertical - 2) * gameProperties.step,
            "player",
            "player_walk_01"
        );
        player.scale.set(1, 1);
        player.anchor.set(0.5, -0.05); //0,15, -0.05
        player.animations.add("walk", ["player_walk_01", "player_walk_02", "player_walk_03", "player_walk_04", "player_walk_05", "player_walk_06", "player_walk_07", "player_walk_08", "player_walk_09", "player_walk_10", "player_walk_01"]);
        player.animations.add("startPush", ["player_push_01", "player_push_02", "player_push_03", "player_push_04", "player_push_05", "player_push_06"]);
        player.animations.add("push", ["player_push_07", "player_push_08", "player_push_09", "player_push_10", "player_push_11"]);
        player.animations.add("push2", ["player_push_15", "player_push_16", "player_push_17", "player_push_18", "player_push_07"]);
        player.animations.add("longPush", ["player_push_07", "player_push_08", "player_push_09", "player_push_10", "player_push_11", "player_push_07", "player_push_08", "player_push_09", "player_push_10", "player_push_11", "player_push_07"]);
        player.animations.add("longPush2", ["player_push_15", "player_push_16", "player_push_17", "player_push_18", "player_push_07", "player_push_08", "player_push_09", "player_push_10", "player_push_11", "player_push_07", "player_push_08"]);
        player.animations.add("endPush", ["player_push_12", "player_push_13", "player_push_14"]);
        player.animations.add("jump", ["player_jump_01", "player_jump_02", "player_jump_03", "player_jump_04", "player_jump_05", "player_jump_06", "player_jump_06", "player_jump_07", "player_jump_07", "player_jump_08", "player_jump_08", "player_jump_09", "player_jump_09", "player_jump_09", "player_jump_09", "player_jump_10", "player_jump_10", "player_jump_11", "player_jump_11", "player_jump_12", "player_jump_13"]);
        player.animations.add("jumpopush", ["player_jump_01", "player_jump_02", "player_jump_03", "player_jump_04", "player_jump_05", "player_push_07", "player_push_08", "player_push_09", "player_push_10", "player_push_11", "player_push_10", "player_push_09", "player_push_10", "player_push_12", "player_push_12", "player_push_13", "player_push_14"]);
        player.animations.add("jumpOf", ["player_down_01", "player_down_02", "player_down_03", "player_down_04", "player_down_05", "player_down_06", "player_down_07", "player_down_08", "player_down_09", "player_down_10", "player_down_11", "player_down_11", "player_down_12", "player_down_13", "player_down_14", "player_down_15", "player_down_16", "player_down_17", "player_down_18", "player_down_19", "player_down_20", "player_down_21"]);
        player.animations.add("fall", ["player_down_11", "player_down_12", "player_down_12", "player_down_12", "player_down_13", "player_down_13", "player_down_13", "player_down_12", "player_down_12", "player_down_12", "player_down_13", "player_down_13", "player_down_13", "player_down_14", "player_down_14", "player_down_15", "player_down_16", "player_down_17", "player_down_18", "player_down_19", "player_down_20", "player_down_21"]);
        player.animations.add("turn", ["player_turn_01", "player_turn_02", "player_turn_03", "player_turn_04", "player_turn_05", "player_turn_06", "player_turn_07", "player_turn_08", "player_turn_09", "player_turn_10", "player_turn_11", "player_turn_12", "player_turn_13", "player_turn_14", "player_turn_15", "player_turn_16", "player_turn_17", "player_turn_18", "player_turn_19", "player_turn_20", "player_turn_21"]);
        player.animations.add("halfTurn", ["player_turn_01", "player_turn_02", "player_turn_03", "player_turn_04", "player_turn_05", "player_turn_06", "player_turn_07", "player_turn_08", "player_turn_09", "player_turn_10", "player_turn_11", "player_turn_12"]);

        player.animations.add("boxing", ["boxing_01", "boxing_02", "boxing_03", "boxing_04", "boxing_05", "boxing_06", "boxing_07", "boxing_08", "boxing_09", "boxing_10", "boxing_11", "boxing_12", "boxing_13", "boxing_14", "boxing_15", "boxing_16", "boxing_17", "boxing_18", "boxing_19", "boxing_20", "boxing_21", "boxing_22"]);
        player.animations.add("next_level", ["next_level_01", "next_level_02", "next_level_03", "next_level_04", "next_level_05", "next_level_06", "next_level_07", "next_level_08", "next_level_09", "next_level_10", "next_level_11", "next_level_12", "next_level_13", "next_level_14"]);
        player.animations.add("remote", ["remote_01", "remote_02", "remote_03", "remote_04", "remote_05", "remote_06", "remote_07", "remote_08", "remote_09", "remote_10", "remote_11", "remote_12", "remote_12", "remote_11", "remote_10", "remote_09", "remote_08", "remote_07", "remote_06", "remote_05", "remote_04", "remote_03", "remote_02", "remote_01"]);
        player.umbrella = game.add.sprite(0, 0, "textures_02", "umbrella");
        player.umbrella.name = "umbrella";
        player.umbrella.anchor.set(0.5, 0.4);
        player.umbrella.renderable = 0;
        player.addChild(player.umbrella);
        player["isPushing"] = false;
    },
    initBoard: function () {
        playBoard = [];

        for (let i = 0; i < gameProperties.numberOfBoxes.horizontal; i++) {
            let rowArray = [];
            for (let j = 0; j < gameProperties.numberOfBoxes.vertical; j++) {
                rowArray.push({
                    SYMBOL: SYMBOL.EMPTY,
                    sprite: null,
                });
            }
            playBoard.push(rowArray);
        }
        playBoard[playerProperties.position.x][playerProperties.position.y].SYMBOL = SYMBOL.PLAYER;
        playBoard[playerProperties.position.x][playerProperties.position.y].sprite = true;
        playBoard[playerProperties.position.x][playerProperties.position.y - 1].SYMBOL = SYMBOL.PLAYER;
        playBoard[playerProperties.position.x][playerProperties.position.y - 1].sprite = true;
        this.updatePlayerPosition();

        // let startPos = gameProperties.numberOfBoxes.vertical - 1;
        // let startBoxes = [
        //     [
        //         {x: 3, y: startPos},
        //         {x: 9, y: startPos},
        //         {x: 11, y: startPos},
        //         {x: 12, y: startPos},
        //         {
        //             x: 12,
        //             y: startPos - 1,
        //         },
        //     ],
        //     [
        //         {x: 0, y: startPos},
        //         {x: 1, y: startPos},
        //         {x: 4, y: startPos},
        //         {x: 9, y: startPos},
        //         {
        //             x: 10,
        //             y: startPos,
        //         },
        //         {x: 10, y: startPos - 1},
        //     ],
        //     [
        //         {x: 1, y: startPos},
        //         {x: 8, y: startPos},
        //         {x: 11, y: startPos},
        //     ],
        //     [
        //         {x: 0, y: startPos},
        //         {x: 1, y: startPos},
        //         {x: 0, y: startPos - 1},
        //         {x: 4, y: startPos},
        //         {
        //             x: 10,
        //             y: startPos,
        //         },
        //     ],
        //     [
        //         {x: 0, y: startPos},
        //         {x: 0, y: startPos - 1},
        //         {x: 0, y: startPos - 2},
        //         {x: 1, y: startPos},
        //         {
        //             x: 2,
        //             y: startPos,
        //         },
        //         {x: 2, y: startPos - 1},
        //         {x: 3, y: startPos},
        //     ],
        //     // [{x: 0, y: startPos}, {x: 1, y: startPos}, {x: 2, y: startPos}, {x: 3, y: startPos}, {x: 4, y: startPos}, {x: 5, y: startPos}, {x: 7, y: startPos}, {
        //     //     x: 8,
        //     //     y: startPos
        //     // }, {x: 9, y: startPos}, {x: 10, y: startPos}, {x: 11, y: startPos}, {x: 12, y: startPos}],
        // ];

        for (let i = 0; i < currentLevel.startBoard.BOX.length; i++) {
            this.initFloorBoxes(currentLevel.startBoard.BOX[i], SYMBOL.BOX);
        }
        for (let i = 0; i < currentLevel.startBoard.BRICK.length; i++) {
            this.initFloorBoxes(currentLevel.startBoard.BRICK[i], SYMBOL.BRICK);
        }
        for (let i = 0; i < currentLevel.startBoard.EXPLOSION.length; i++) {
            this.initFloorBoxes(currentLevel.startBoard.EXPLOSION[i], SYMBOL.EXPLOSION);
        }


        // //////////////////////////////
        // for (let i = 0; i < gameProperties.numberOfBoxes.horizontal; i++) {
        //     for (let j = 1; j < gameProperties.numberOfBoxes.vertical; j++) {
        //         // game.add.sprite(gameProperties.startPosition.x + (i * gameProperties.step), gameProperties.startPosition.y + (j * gameProperties.step), 'textures', 'box01');
        //         if (playBoard[i][j].SYMBOL == SYMBOL.EMPTY && j > 3) {
        //             playBoard[i][j].SYMBOL = SYMBOL.BOX;
        //         }
        //     }
        // }
    },
    initScoreBoard: function () {
        this.scoreGroup = game.add.group();
        let scoreBoard = game.add.sprite(1564, 32, "textures_02", "point_table");
        scoreBoard.name = "scoreBoard";
        this.totalScore = game.add.bitmapText(1664, 46, 'led_font', 0, 36);
        this.nextLevelScore = game.add.bitmapText(1732, 90, 'led_font', 0, 36);
        this.scoreGroup.add(scoreBoard);
        this.scoreGroup.add(this.totalScore);
        this.scoreGroup.add(this.nextLevelScore);
        this.updateScore();
        this.initLives();
        this.scoreGroup.add(this.lifeGroup);
        this.scoreGroup.alpha = 0.9

    },
    updateScore: function () {
        this.totalScore.text = currentScore + currentTotalScore;
        this.nextLevelScore.text = (currentLevel.levelCompleteScore - currentScore < 0) ? 0 : currentLevel.levelCompleteScore - currentScore;
        game.world.bringToTop(this.scoreGroup);
    },
    initLives: function () {
        this.lifeGroup = game.add.group();
        this.lifeGroup.name = "lifeGroup";
        this.lifeGroup.alpha = 0.9;
        for (let i = 0; i < life.max; i++) {
            let lifeSprite = game.add.sprite(0, -4, "textures_02", "life_0" + (i + 1));
            lifeSprite.anchor.set(1, 0);
            lifeSprite.x = i * 68;
            lifeSprite.valid = true;
            this.lifeGroup.add(lifeSprite)
        }
        for (let i = life.max; i > life.value; i--) {
            let lifeSprite = this.lifeGroup.children[life.max - i];
            lifeSprite.valid = false;
            lifeSprite.renderable = false;
        }
        this.lifeGroup.position.set(1730, 152);
    },
    loseLifeSprite: function () {
        for (let i = life.max; i > life.value; i--) {
            let lifeSprite = this.lifeGroup.children[life.max - i];
            if (lifeSprite.valid == true) {
                lifeSprite.valid = false;
                game.add.tween(lifeSprite).to({rotation: -Math.PI / 8}, 540, Phaser.Easing.Sinusoidal.In, true);
                game.add.tween(lifeSprite).to({y: lifeSprite.y + 100}, 400, Phaser.Easing.Cubic.In, true, 140);
                game.add
                    .tween(lifeSprite)
                    .to({alpha: 0}, 200, Phaser.Easing.Linear.None, true, 350)
                    .onComplete.add(() => {
                    lifeSprite.renderable = false;
                });
            }

        }
    },
    initSounds: function () {
        if (mute) {
            soundOff.inputEnabled = true;
            soundOff.input.useHandCursor = true;
            if (soundOn.inputEnabled != null) {
                soundOn.inputEnabled = false;
                soundOn.input.useHandCursor = false;
            }
            soundOn.alpha = 0;
            soundOff.alpha = 1;
        } else {
            soundOn.inputEnabled = true;
            soundOn.input.useHandCursor = true;
            if (soundOff.inputEnabled != null) {
                soundOff.inputEnabled = false;
                soundOff.input.useHandCursor = false;
            }
            soundOff.alpha = 0;
            soundOn.alpha = 1;
        }

        tapeSound = game.add.audio("tape");
        tapeSound.allowMultiple = true;

        soundOn.events.onInputDown.add(() => {
            tapeSound.play();
            mute = true;
            setMute();
            soundOn.inputEnabled = false;
            soundOn.input.useHandCursor = false;
            soundOff.inputEnabled = true;
            soundOff.input.useHandCursor = true;
            soundOn.alpha = 0;
            soundOff.alpha = 1;
        }, this);
        soundOff.events.onInputDown.add(() => {
            mute = false;
            PlaySound(tapeSound);
            setMute();
            soundOff.inputEnabled = false;
            soundOff.input.useHandCursor = false;
            soundOn.inputEnabled = true;
            soundOn.input.useHandCursor = true;
            soundOff.alpha = 0;
            soundOn.alpha = 1;
        }, this);

        angelsSound = game.add.audio("angels");

        hitSound = game.add.audio("hit");
        hitSound.allowMultiple = true;

        jumpSound = game.add.audio("jump");
        jumpSound.allowMultiple = true;

        jumpOfSound = game.add.audio("jump_of");
        jumpOfSound.allowMultiple = true;

        pushSound = game.add.audio("push");
        pushSound.allowMultiple = true;

        fartSound = game.add.audio("fart");
        fartSound.allowMultiple = true;

        walkSound = game.add.audio("walk");
        walkSound.allowMultiple = true;

        boxDropSound = game.add.audio("boxDrop");
        boxDropSound.allowMultiple = true;

        boxSmashSound = game.add.audio("boxSmash");
        boxSmashSound.allowMultiple = true;

        swearSound = game.add.audio("swear");
        swearSound.allowMultiple = true;


        clawSound = game.add.audio("menuIn");
        clawSound.allowMultiple = false;

        chainSound = game.add.audio("chain");
        chainSound.allowMultiple = false;

        explosionSound = game.add.audio("explosion");
        explosionSound.allowMultiple = true;

        lawnmowerSound = game.add.audio("lawnmower");
        lawnmowerSound.allowMultiple = true;


        boxMowSound_0 = game.add.audio("box_mow_0");
        boxMowSound_0.allowMultiple = true;


        boxMowSound_1 = game.add.audio("box_mow_1");
        boxMowSound_1.allowMultiple = true;

        springsSound = game.add.audio("springs");
        springsSound.allowMultiple = true;

        itemSound = game.add.audio("item");
        itemSound.allowMultiple = true;

        pointsSound = game.add.audio("points");
        pointsSound.allowMultiple = true;

    },
    initShelf: function () {
        this.shelf = game.add.group();
        this.shelfInside = game.add.group();
        this.eqBox = game.add.sprite(0, 0, "eq", "box_eq_01");
        this.eqBox.anchor.y = 0.5;
        this.eqBox.scale.set(0.8, 0.8);
        this.eqBox.animations.add("open", ["box_eq_01", "box_eq_02", "box_eq_03", "box_eq_04"]);
        this.eqBox.animations.add("close", ["box_eq_04", "box_eq_03", "box_eq_02", "box_eq_01"]);

        this.shelf.position.set(6, 1010);

        this.shelfBg = game.add.sprite(0, 0, "textures_02", "box_ui_2");
        this.shelfBg.scale.y = 1.3;
        this.shelfBg.width = 0;
        this.shelfBg.anchor.y = 0.5;
        this.shelfBg.position.set(this.eqBox.width - 10, 20);
        this.shelf.add(this.shelfBg);

        this.shelfEnd = game.add.sprite(0, 0, "textures_02", "box_ui_1");
        this.shelfEnd.scale.set(1.3, 1.3);
        this.shelfEnd.anchor.y = 0.5;
        this.shelfEnd.position.set(this.shelfBg.x + this.shelfBg.width - 0.2, this.shelfBg.y);
        this.shelf.add(this.shelfEnd);
        this.shelf.add(this.shelfInside);
        this.shelf.add(this.eqBox);

        this.skills = {
            lawnmower: game.add.sprite(0, 0, "eq", "remote_eq_01"),
            boxing: game.add.sprite(0, 0, "eq", "boxing_eq_01"),
            springs: game.add.sprite(0, 0, "eq", "shoes_eq_01"),
            hammer: game.add.sprite(0, 0, "eq", "hammer_eq_01"),
            umbrella: game.add.sprite(0, 0, "eq", "umbrella_eq_01"),
        };
        this.skills.lawnmower.name = "lawnmower";
        this.skills.boxing.name = "boxing";
        this.skills.springs.name = "springs";
        this.skills.hammer.name = "hammer";
        this.skills.umbrella.name = "umbrella";
        this.skills.lawnmower.animations.add("skill", ["remote_eq_01", "remote_eq_02", "remote_eq_03", "remote_eq_04", "remote_eq_05", "remote_eq_06", "remote_eq_07"]);
        this.skills.boxing.animations.add("skill", ["boxing_eq_01", "boxing_eq_02", "boxing_eq_03", "boxing_eq_04", "boxing_eq_03", "boxing_eq_02", "boxing_eq_01"]);
        this.skills.springs.animations.add("skill", ["shoes_eq_01", "shoes_eq_02", "shoes_eq_03", "shoes_eq_04", "shoes_eq_05", "shoes_eq_06", "shoes_eq_05", "shoes_eq_04", "shoes_eq_03", "shoes_eq_02", "shoes_eq_01"]);
        this.skills.hammer.animations.add("skill", ["hammer_eq_01", "hammer_eq_02", "hammer_eq_03", "hammer_eq_04", "hammer_eq_01"]);
        this.skills.umbrella.animations.add("skill", ["umbrella_eq_01", "umbrella_eq_02", "umbrella_eq_03", "umbrella_eq_02", "umbrella_eq_01"]);
        this.shelfInside.add(this.skills.lawnmower);
        this.shelfInside.add(this.skills.boxing);
        this.shelfInside.add(this.skills.springs);
        this.shelfInside.add(this.skills.hammer);
        this.shelfInside.add(this.skills.umbrella);
        for (let i = 0; i < this.shelfInside.children.length; i++) {
            this.shelfInside.children[i].anchor.set(0.5, 0.5);
            this.shelfInside.children[i].scale.set(0.65, 0.65);
            this.shelfInside.children[i].y = 24;
            let text = game.add.text(16, -2, "0", {
                fontSize: '60px',
                fill: "#000000",
                stroke: "#ffffff",
                strokeThickness: 2
            });
            this.shelfInside.children[i].addChild(text);
            this.shelfInside.children[i].alpha = 0;
            this.shelfInside.children[i].inputEnabled = true;
            this.shelfInside.children[i].input.useHandCursor = false;
            let element = this.shelfInside.children[i];
            this.shelfInside.children[i].events.onInputDown.add(() => {
                if (gameProperties.end || playerProperties.isMoveBlocked || !playerProperties.canUseSkill) {
                    return;
                }
                switch (i) {
                    case 0: {
                        this.skills.lawnmower.animations.play("skill", 20, false);
                        this.lawnmower();
                        break;
                    }
                    case 1: {
                        this.skills.boxing.animations.play("skill", 20, false);
                        this.boxing();
                        break;
                    }
                    case 2: {
                        this.skills.springs.animations.play("skill", 20, false);
                        this.springs();
                        break;
                    }
                    case 3: {
                        this.skills.hammer.animations.play("skill", 12, false);
                        this.hammer();
                        break;
                    }
                    case 4: {
                        if (equipment.umbrella > 0) {
                            this.skills.umbrella.animations.play("skill", 12, false);
                            equipmentActive.umbrella = !equipmentActive.umbrella;
                            this.setActive(element, equipmentActive.umbrella);
                            this.setUmbrella(equipmentActive.umbrella);
                        }
                        break;
                    }
                }
            }, this);
        }

        this.shelfInside.removeAll();
        this.updateShelf(true);
        if (equipment.umbrella > 0) {
            this.shelfInside.children.forEach(child => {
                if (child.name === "umbrella") {
                    this.setActive(child, equipmentActive.umbrella);
                    this.setUmbrella(equipmentActive.umbrella);
                }
            })

        }

    },
    eqUsed: function () {
        this.addToScore(currentLevel.scorePerItemUse);
    },
    updateShelf: function (start) {
        if (!start) {
            start = false
        }
        let isOpen = false;
        const between = 140, fromBox = 250;
        if (this.shelfInside.children.length > 0) {
            isOpen = true;
        }
        for (let i in equipment) {
            if (equipment[i] > 0) {
                this.shelfInside.add(this.skills[i]);
                if (this.skills[i].uiSelection) {
                    this.skills[i].uiSelection.position.set(this.skills[i].x, this.skills[i].y);
                }
                if (this.skills[i].children[0].setText) {
                    this.skills[i].children[0].setText(equipment[i].toString());
                } else {
                    this.skills[i].children[1].setText(equipment[i].toString());
                }
                if (this.skills[i].alpha === 0) {
                    game.add
                        .tween(this.skills[i])
                        .to({alpha: 1}, 200, Phaser.Easing.Linear.None, true);
                } else {
                    this.skills[i].alpha = 1;
                }
            } else {
                if (this.skills[i].uiSelection) {
                    equipmentActive[i] = !equipmentActive[i];
                    this.setActive(this.skills[i], equipmentActive[i]);
                    if (i === "umbrella") {
                        this.setUmbrella(equipmentActive[i]);
                    }
                }
                if (this.skills[i].alpha > 0) {
                    game.add
                        .tween(this.skills[i])
                        .to({alpha: 0}, 200, Phaser.Easing.Linear.None, true)
                        .onComplete.add(() => {
                        this.updateShelf();
                    });
                } else {
                    this.shelfInside.remove(this.skills[i])
                }
            }
        }
        for (let i = 0; i < this.shelfInside.children.length; i++) {
            this.shelfInside.children[i].x = i * between + fromBox;
        }
        if (this.shelfInside.children.length > 0) {
            if (isOpen === false) {
                this.eqBox.play("open", 10);
            }
        } else {
            if (isOpen === true) {
                this.eqBox.play("close", 10);
            }
        }
        if (start) {
            this.shelfBg.width = this.shelfInside.width + 50;
            this.shelfEnd.position.set(this.shelfBg.x + this.shelfBg.width, this.shelfBg.y);
            return;
        }
        game.add
            .tween(this.shelfBg)
            .onUpdateCallback(() => {
                this.shelfEnd.position.set(this.shelfBg.x + this.shelfBg.width, this.shelfBg.y);
            })
            .to({width: this.shelfInside.width + 50}, 50, Phaser.Easing.Linear.None, true)
            .onComplete.add(() => {
            this.shelfBg.width = this.shelfInside.width + 50;
            this.shelfEnd.position.set(this.shelfBg.x + this.shelfBg.width, this.shelfBg.y);

        });
    },
    getEquipment: function () {
        if (getRandomInt(1, 100) <= currentLevel.itemChance) {
            let eqArray = [];
            for (let i in currentLevel.chancesPerItem) {
                if (currentLevel.chancesPerItem.hasOwnProperty(i)) {
                    for (let j = 0; j < currentLevel.chancesPerItem[i]; j++) {
                        eqArray.push(i);
                    }
                }
            }
            PlaySound(itemSound);
            equipment[eqArray[getRandomInt(0, eqArray.length - 1)]]++;
            this.updateShelf();
        }
    },
    lawnmower: function () {
        if (equipment.lawnmower === 0 || !playerProperties.canUseSkill) {
            return;
        }
        equipment.lawnmower--;
        this.eqUsed();
        this.updateShelf();
        debugLog("lawnmower left: " + equipment.lawnmower);
        playerProperties.isMoveBlocked = true;
        playerProperties.canUseSkill = false;


        player.animations.getAnimation("remote").onComplete.addOnce(() => {
                playerProperties.isMoveBlocked = false;
                PlaySound(lawnmowerSound);

                let lawnmower = game.add.sprite(
                    gameProperties.startPosition.x + (gameProperties.numberOfBoxes.horizontal - 1) * gameProperties.step,
                    gameProperties.startPosition.y + (gameProperties.numberOfBoxes.vertical) * gameProperties.step,
                    "textures_02",
                    "mower_01"
                );
                lawnmower.scale.set(0.5, 0.5);
                lawnmower.anchor.x = 0.5;
                lawnmower.y -= lawnmower.height - 20;
                lawnmower.x += 2 * lawnmower.width;
                lawnmower.animations.add("mower", ["mower_01", "mower_02", "mower_03"]);
                lawnmower.play("mower", 20, true);

                let checkPosition = [], lowerRow = gameProperties.numberOfBoxes.vertical - 1;
                for (let j = 0; j < gameProperties.numberOfBoxes.horizontal; j++) {
                    checkPosition.push(false);
                }

                game.add
                    .tween(lawnmower)
                    .onUpdateCallback(() => {
                        game.world.bringToTop(lawnmower);
                        game.world.bringToTop(this.shelf);

                        for (let j = 0; j < gameProperties.numberOfBoxes.horizontal; j++) {
                            if (lawnmower.x >= gameProperties.startPosition.x + j * gameProperties.step - 30 && lawnmower.x <= gameProperties.startPosition.x + j * gameProperties.step + 30) {
                                if (this.isBoxType(j, lowerRow) && !checkPosition[j]) {
                                    checkPosition[j] = true;
                                    this.destroyBox(j, lowerRow);
                                    (getRandomInt(0, 1) === 0) ? PlaySound(boxMowSound_0) : PlaySound(boxMowSound_1);
                                    this.checkBoxes();
                                    this.movePlayer(Phaser.Keyboard.DOWN)

                                }
                            }
                        }
                    })
                    .to({x: gameProperties.startPosition.x - lawnmower.width}, 2000, Phaser.Easing.Linear.None, true, 500)
                    .onComplete.add(() => {
                    playerProperties.canUseSkill = true;

                    lawnmower.animations.stop("mower");
                    lawnmower.destroy();
                });

            }
        );
        player.animations.play("remote", playerProperties.animationFrameRate);

    },
    boxing: function () {
        if (equipment.boxing === 0 || !playerProperties.canUseSkill) {
            return;
        }

        playerProperties.isMoveBlocked = true;
        playerProperties.canUseSkill = false;

        let box = false;
        PlaySound(pushSound);
        player.animations.getAnimation("boxing").enableUpdate = true;
        player.animations.getAnimation("boxing").onUpdate.add((anim, frame) => {
            if (frame.index > 11 && !box) {
                box = true;
                let facing = (playerProperties.isPlayerFacingRight) ? 1 : -1,
                    endComparing = (playerProperties.isPlayerFacingRight) ? gameProperties.numberOfBoxes.horizontal : 0,
                    isLegalBoxing = false;

                if (playerProperties.isPlayerFacingRight) {
                    if (playerProperties.position.x < endComparing - 1 && playBoard[playerProperties.position.x + facing][playerProperties.position.y].SYMBOL === SYMBOL.BOX) {
                        isLegalBoxing = true;
                    }
                } else {
                    if (playerProperties.position.x > endComparing && playBoard[playerProperties.position.x + facing][playerProperties.position.y].SYMBOL === SYMBOL.BOX) {
                        isLegalBoxing = true;
                    }
                }

                if (isLegalBoxing) {
                    equipment.boxing--;
                    this.eqUsed();
                    this.updateShelf();
                    debugLog("boxing left: " + equipment.boxing);
                    PlaySound(boxSmashSound);
                    PlaySound(boxDropSound);
                    PlaySound(fartSound);

                    PlaySound(walkSound);
                    this.destroyBox(playerProperties.position.x + facing, playerProperties.position.y);
                    this.checkBoxes();
                    playerProperties.canUseSkill = true;
                }
            }
        }, this);
        player.animations.getAnimation("boxing").onComplete.addOnce(() => {
                playerProperties.isMoveBlocked = false;
                playerProperties.canUseSkill = true;
            }
        );
        player.animations.play("boxing", playerProperties.animationFrameRate);

    },

    createUICircle: function () {
        let circle = game.add.graphics(0, 0);
        circle.lineStyle(2, 0x000000, 1);
        circle.beginFill(0xfcfc20, 1);
        circle.drawEllipse(0, 0, 40, 30);
        circle.alpha = 0.3;
        return circle;
    },
    setActive: function (element, isActive) {
        if (isActive) {
            element.uiSelection = this.createUICircle();
            element.uiSelection.position.set(element.x, element.y);
            element.parent.parent.addChildAt(element.uiSelection, 0);
        } else {
            if (element.uiSelection) {
                element.uiSelection.parent.removeChild(element.uiSelection);
                element.uiSelection.destroy();
                element.uiSelection = null;
            }
        }
    },
    setUmbrella: function (isActive) {
        if (isActive) {
            if (player.umbrella.renderable == false) {
                player.umbrella.alpha = 0;
                player.umbrella.renderable = true;
                game.add.tween(player.umbrella).to({alpha: 1,}, 200, Phaser.Easing.Linear.None, true);
            }
        } else {
            if (player.umbrella.renderable == true) {
                game.add.tween(player.umbrella).to({alpha: 0,}, 200, Phaser.Easing.Linear.None, true).onComplete.add(() => {
                    player.umbrella.alpha = 0;
                    player.umbrella.renderable = false;
                });
            }
        }
    },
    springs: function () {
        if (equipment.springs === 0 || !playerProperties.canUseSkill) {
            return;
        }
        if (this.movePlayer("SPRINGS")) {
            PlaySound(springsSound);
            equipment.springs--;
            this.eqUsed();
            this.updateShelf();
            debugLog("springs left: " + equipment.springs);

            playerProperties.isMoveBlocked = true;
            playerProperties.canUseSkill = false;


            let time = playerProperties.animationTime * 1.4;
            this.springsExplode();
            this.springsTrails(time);

            setTimeout(() => {
                playerProperties.isMoveBlocked = false;
                playerProperties.canUseSkill = true;
            }, time);


        }

    },
    springsExplode: function () {
        let emitter = game.add.emitter(0, 0);

        emitter.makeParticles("textures_01", ["cloud_01", "cloud_02", "cloud_03", "cloud_04", "cloud_05"]);
        emitter.width = 100;
        emitter.setXSpeed(-100, 100);
        emitter.setYSpeed(-75, -140);
        emitter.gravity = 300;
        emitter.bringToTop = true;
        emitter.setScale(0.4, 0.6, 0.4, 0.6, 1000, Phaser.Easing.Linear.None, false);
        emitter.minRotation = -360;
        emitter.maxRotation = 360;
        emitter.setAlpha(1, 0, 1000);
        emitter.emitX = player.position.x;
        emitter.emitY = player.position.y + player.height;
        emitter.start(true, 2000, 1, 20);
        if (emitter != null) {
            setTimeout(() => {
                emitter.destroy();
            }, 2000);
        }
    },
    springsTrails: function (time) {
        let emitter = game.add.emitter(0, 0);

        emitter.makeParticles("textures_01", ["cloud_01", "cloud_02", "cloud_03", "cloud_04", "cloud_05"]);
        emitter.width = 10;
        emitter.setXSpeed(-100, 100);
        emitter.setYSpeed(-75, -140);
        emitter.gravity = 300;
        emitter.bringToTop = true;
        emitter.setScale(0.4, 0.6, 0.4, 0.6, 1000, Phaser.Easing.Linear.None, false);
        emitter.minRotation = -360;
        emitter.maxRotation = 360;
        emitter.setAlpha(1, 0, 1000);
        emitter.start(false, 2000, 1, 1000);
        let emitterObj = {x: 1};
        game.add.tween(emitterObj).to(
            {
                x: 0,
            },
            time * 0.65,
            Phaser.Easing.Linear.None,
            true
        ).onUpdateCallback(() => {
            emitter.emitX = player.position.x;
            emitter.emitY = player.position.y + player.height;
        }, "emitter").onComplete.add(() => {
            if (emitter != null) {
                emitter.emitX = -1000;
                emitter.emitY = -1000;
                setTimeout(() => {
                    emitter.destroy();
                }, 2000);
            }

        });
    },
    umbrella: function () {
        if (equipment.umbrella === 0) {
            return false;
        }
        if (!equipmentActive.umbrella) {
            return false;
        }
        equipment.umbrella--;
        this.eqUsed();
        this.updateShelf();
        debugLog("umbrella left: " + equipment.umbrella);
        return true;
    },
    hammer: function () {
        if (equipment.hammer === 0 || !playerProperties.canUseSkill) {
            return;
        }

        playerProperties.isMoveBlocked = true;
        playerProperties.canUseSkill = false;

        let bricksArray = [];
        for (let i = gameProperties.numberOfBoxes.horizontal - 1; i >= 0; i--) {
            for (let j = gameProperties.numberOfBoxes.vertical - 1; j >= 0; j--) {
                if (playBoard[i][j].SYMBOL === SYMBOL.BRICK && !playBoard[i][j].sprite.isFalling) {
                    bricksArray.push({x: i, y: j})
                }
            }
        }
        if (bricksArray.length === 0) {
            playerProperties.isMoveBlocked = false;
            playerProperties.canUseSkill = true;
            return;
        }

        const bricksToDestroy = bricksArray[getRandomInt(0, bricksArray.length - 1)];
        bricksArray = null;
        equipment.hammer--;
        this.eqUsed();
        this.updateShelf();
        debugLog("hammer left: " + equipment.hammer);

        let hammer = game.add.sprite(-500, -500, "eq", "hammer_eq_01");
        hammer.anchor.set(0.5, 0.5);
        let randomLocation = [
            {x: -100, y: game.world.height * 0.1},
            {x: -100, y: game.world.height * 0.3},
            {x: -100, y: game.world.height * 0.6},
            {x: game.world.width + 100, y: game.world.height * 0.2},
            {x: game.world.width + 100, y: game.world.height * 0.4},
            {x: game.world.width + 100, y: game.world.height * 0.7},
            {x: game.world.width * 0.1, y: -100},
            {x: game.world.width * 0.3, y: -100},
            {x: game.world.width * 0.4, y: -100},
            {x: game.world.width * 0.6, y: -100}
            , {x: game.world.width * 0.8, y: -100}
        ];
        let randomPosition = randomLocation[getRandomInt(0, randomLocation.length - 1)];
        hammer.position.set(randomPosition.x, randomPosition.y);
        let target = playBoard[bricksToDestroy.x][bricksToDestroy.y].sprite;

        let speed = 24,
            time = distance(target.x + target.width / 2, target.y + target.height / 2, hammer.x, hammer.y) / speed * game.time.physicsElapsed;

        game.add
            .tween(hammer)
            .to({
                x: target.x + target.width / 2,
                y: target.y + target.height / 2,
                rotation: Math.PI * 6
            }, time * 1000, Phaser.Easing.Linear.None, true).onComplete.add(
            () => {
                hammer.parent.removeChild(hammer);
                hammer.destroy();


                PlaySound(boxSmashSound);
                PlaySound(boxDropSound);
                this.destroyBox(bricksToDestroy.x, bricksToDestroy.y);
                this.checkBoxes();
                this.movePlayer(Phaser.Keyboard.DOWN);
                playerProperties.isMoveBlocked = false;
                playerProperties.canUseSkill = true;
            });


    }
};
