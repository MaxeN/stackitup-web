GameManager.NewGame.prototype = {
    create: function () {
        levelIndex = 0;
        currentTotalScore = 0;
        life = {value: 3, max: 3};
        equipment = {
            lawnmower: 0,
            boxing: 0,
            springs: 0,
            hammer: 0,
            umbrella: 0
        };
        equipmentActive = {
            umbrella: false
        };

        currentMusicIndex = getRandomInt(0, 4);
        playBackgroundMusic();
        setTimeout(() => {
            game.state.start("Game");
        }, 100);
    },


};
