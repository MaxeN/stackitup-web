GameManager.Intro.prototype = {

    create: function () {
        let video = game.add.video('intro');
        const proportion = game.world.width / video.width;
        video.addToWorld(game.world.width / 2, game.world.height / 2, 0.5, 0.5, proportion, proportion);
        let skip = game.add.sprite(game.world.width - 20, game.world.height - 20, "textures_03", "skip_button");
        skip.anchor.set(1, 1);
        skip.alpha = 0.65;
        skip.inputEnabled = true;
        skip.input.useHandCursor = true;
        skip.events.onInputDown.add(() => {
            video.stop();
            this.onIntroCompleted(skip);
        });
        video.onComplete.add(() => {
            this.onIntroCompleted(skip);
        });
        video.play(false);

    },
    onIntroCompleted: function (skip) {
        skip.inputEnabled = false;
        skip.input.useHandCursor = false;
        let overlay = game.add.graphics();
        overlay.beginFill(0x000000, 1);
        overlay.drawRect(0, 0, game.world.width, game.world.height);
        overlay.endFill();
        overlay.alpha = 0;
        game.world.bringToTop(overlay);
        game.add.tween(overlay).to({alpha: 1}, 400, Phaser.Easing.Linear.None, true).onComplete.add(() => {
            game.add.sprite(0, 0, 'background');
            game.add.sprite(0, game.world.height - 89, 'textures', 'ground');
            game.add.sprite(-12, 0, 'textures', 'wallLeft');
            game.add.sprite(game.world.width - 74, 0, 'textures', 'wallRight');
            game.add.sprite(game.world.width / 2, game.world.height / 2 + 140, 'logo').anchor.set(0.5, 0.5);
            game.world.bringToTop(overlay);
            game.add.tween(overlay).to({alpha: 0}, 400, Phaser.Easing.Linear.None, true).onComplete.add(() => {
                game.state.start('Menu');
            });
        });
    }
};