levelIndex = 0;
currentTotalScore = 0;
equipment = {
    lawnmower: 0,
    boxing: 0,
    springs: 0,
    hammer: 0,
    umbrella: 0,
};
equipmentActive = {
    springs: false,
    hammer: false,
    umbrella: false
};
currentMusicIndex = 0;
musicGame = null;
SYMBOL = {EMPTY: 0, BOX: 1, PLAYER: 2, BRICK: 3, EXPLOSION: 4};
levels = [
    {
        assets: {
            background: "background_base",
            boxesSprites: [
                {name: "box01", atlas: "textures"},
                {name: "box02", atlas: "textures"},
                {name: "box03", atlas: "textures"},
                {name: "box04", atlas: "textures"},
                {name: "box05", atlas: "textures"}
            ],
            leftWall: {name: "wallLeft", atlas: "textures", offset: -12},
            rightWall: {name: "wallRight", atlas: "textures", offset: -74},
            ground: {name: "ground", atlas: "textures"},
        },

        clawSpeed: 0.7,
        timeOfNextBox: 4500,
        nextBoxOffsetTime: 100,
        minimumTime: 2500,
        scoreForEveryDroppedBox: 10,
        scoreRow: 150,
        scorePerItemUse: 250,
        levelCompleteScore: 2500,
        startBoard: {
            BOX: [
                {x: 3, y: 7},
                {x: 11, y: 7},
                {x: 12, y: 7},
                {x: 12, y: 6},
                {x: 9, y: 7},
            ],
            BRICK: [],
            EXPLOSION: []
        },
        spawnBoxes: [
            {type: SYMBOL.BOX, count: 1},
            {type: SYMBOL.EXPLOSION, count: 0},
            {type: SYMBOL.BRICK, count: 0}
        ],
        itemChance: 25,
        chancesPerItem: {
            lawnmower: 0,
            boxing: 1,
            springs: 1,
            hammer: 0,
            umbrella: 0
        }
    },
    {
        assets: {
            background: "background_food",
            boxesSprites: [
                {name: "box_food_01", atlas: "textures_04"},
                {name: "box_food_02", atlas: "textures_04"},
                {name: "box_food_03", atlas: "textures_04"},
                {name: "box_food_04", atlas: "textures_04"},
                {name: "box_food_05", atlas: "textures_04"},
                {name: "box01", atlas: "textures"},
                {name: "box02", atlas: "textures"},
                {name: "box05", atlas: "textures"}
            ],
            leftWall: {name: "wall_02_left", atlas: "textures_02", offset: -14},
            rightWall: {name: "wall_02_right", atlas: "textures_02", offset: -30},
            ground: {name: "ground_bg_food", atlas: "textures_02"},
        },

        clawSpeed: 0.7,
        timeOfNextBox: 4500,
        nextBoxOffsetTime: 300,
        minimumTime: 1500,
        scoreForEveryDroppedBox: 20,
        scoreRow: 200,
        scorePerItemUse: 300,
        levelCompleteScore: 6000,
        startBoard: {
            BOX: [
                {x: 0, y: 7},
                {x: 0, y: 6},
                {x: 2, y: 7},
                {x: 2, y: 6},
                {x: 7, y: 7},
                {x: 7, y: 6},
                {x: 8, y: 6},

            ],
            BRICK: [
                {x: 8, y: 7},
            ],
            EXPLOSION: []
        },
        spawnBoxes: [
            {type: SYMBOL.BOX, count: 50},
            {type: SYMBOL.EXPLOSION, count: 1},
            {type: SYMBOL.BRICK, count: 0}
        ],
        itemChance: 35,
        chancesPerItem: {
            lawnmower: 0,
            boxing: 3,
            springs: 1,
            hammer: 1,
            umbrella: 1
        }
    },
    {
        assets: {
            background: "background_furniture",
            boxesSprites: [
                {name: "box_furniture_01", atlas: "textures_04"},
                {name: "box_furniture_02", atlas: "textures_04"},
                {name: "box_furniture_03", atlas: "textures_04"},
                {name: "box_furniture_04", atlas: "textures_04"},
                {name: "box_furniture_05", atlas: "textures_04"},
                {name: "box01", atlas: "textures"},
                {name: "box02", atlas: "textures"},
                {name: "box05", atlas: "textures"}
            ],
            leftWall: {name: "wall_02_left", atlas: "textures_02", offset: -14},
            rightWall: {name: "wall_02_right", atlas: "textures_02", offset: -30},
            ground: {name: "ground_bg_furniture", atlas: "textures_02"},
        },

        clawSpeed: 0.75,
        timeOfNextBox: 4500,
        nextBoxOffsetTime: 400,
        minimumTime: 1300,
        scoreForEveryDroppedBox: 40,
        scoreRow: 350,
        scorePerItemUse: 400,
        levelCompleteScore: 12000,
        startBoard: {
            BOX: [
                {x: 1, y: 7},
                {x: 3, y: 7},
                {x: 4, y: 7},
                {x: 4, y: 6},
                {x: 4, y: 5},
                {x: 4, y: 4},
                {x: 8, y: 7},
                {x: 8, y: 6},
            ],
            BRICK: [],
            EXPLOSION: []
        },
        spawnBoxes: [
            {type: SYMBOL.BOX, count: 70},
            {type: SYMBOL.EXPLOSION, count: 3},
            {type: SYMBOL.BRICK, count: 1}
        ],
        itemChance: 60,
        chancesPerItem: {
            lawnmower: 1,
            boxing: 5,
            springs: 2,
            hammer: 1,
            umbrella: 1
        }
    },
    {
        assets: {
            background: "background_car",
            boxesSprites: [
                {name: "box_car_01", atlas: "textures_04"},
                {name: "box_car_02", atlas: "textures_04"},
                {name: "box_car_03", atlas: "textures_04"},
                {name: "box_car_04", atlas: "textures_04"},
                {name: "box_ship_01", atlas: "textures_04"},
                {name: "box_ship_05", atlas: "textures_04"}
            ],
            leftWall: {name: "wallLeft", atlas: "textures", offset: -12},
            rightWall: {name: "wallRight", atlas: "textures", offset: -74},
            ground: {name: "ground_bg_car", atlas: "textures_02"},
        },

        clawSpeed: 0.8,
        timeOfNextBox: 4000,
        nextBoxOffsetTime: 500,
        minimumTime: 1200,
        scoreForEveryDroppedBox: 60,
        scoreRow: 550,
        scorePerItemUse: 800,
        levelCompleteScore: 25000,
        startBoard: {
            BOX: [
                {x: 3, y: 7},
                {x: 4, y: 7},
                {x: 4, y: 6},

            ],
            BRICK: [
                {x: 7, y: 7},
                {x: 12, y: 7},
            ],
            EXPLOSION: []
        },
        spawnBoxes: [
            {type: SYMBOL.BOX, count: 80},
            {type: SYMBOL.EXPLOSION, count: 2},
            {type: SYMBOL.BRICK, count: 2}
        ],
        itemChance: 80,
        chancesPerItem: {
            lawnmower: 2,
            boxing: 3,
            springs: 2,
            hammer: 3,
            umbrella: 2
        }
    },
    {
        assets: {
            background: "background_ship",
            boxesSprites: [
                {name: "box_ship_01", atlas: "textures_04"},
                {name: "box_ship_02", atlas: "textures_04"},
                {name: "box_ship_03", atlas: "textures_04"},
                {name: "box_ship_04", atlas: "textures_04"},
                {name: "box_ship_05", atlas: "textures_04"}
            ],
            leftWall: {name: "wallLeft", atlas: "textures", offset: -12},
            rightWall: {name: "wallRight", atlas: "textures", offset: -74},
            ground: {name: "ground", atlas: "textures"},
        },
        clawSpeed: 0.9,
        timeOfNextBox: 3500,
        nextBoxOffsetTime: 600,
        minimumTime: 1200,
        scoreForEveryDroppedBox: 80,
        scoreRow: 700,
        scorePerItemUse: 1000,
        levelCompleteScore: 40000,
        startBoard: {
            BOX: [
                {x: 1, y: 7},
                {x: 2, y: 7},
                {x: 3, y: 7},
                {x: 4, y: 7},
                {x: 5, y: 7},
                {x: 7, y: 7},
                {x: 8, y: 7},
                {x: 9, y: 7},
                {x: 10, y: 7},
                {x: 11, y: 7},
                {x: 12, y: 7}
            ],
            BRICK: [
                {x: 0, y: 7}
            ],
            EXPLOSION: []
        },
        spawnBoxes: [
            {type: SYMBOL.BOX, count: 70},
            {type: SYMBOL.EXPLOSION, count: 4},
            {type: SYMBOL.BRICK, count: 4}
        ],
        itemChance: 90,
        chancesPerItem: {
            lawnmower: 1,
            boxing: 1,
            springs: 1,
            hammer: 2,
            umbrella: 1
        }
    },
    {
        assets: {
            background: "background_plane",
            boxesSprites: [
                {name: "box_plane_01", atlas: "textures_04"},
                {name: "box_plane_02", atlas: "textures_04"},
                {name: "box_plane_03", atlas: "textures_04"},
                {name: "box_plane_04", atlas: "textures_04"},
                {name: "box_plane_05", atlas: "textures_04"},
                {name: "box_plane_06", atlas: "textures_04"},
                {name: "box_plane_07", atlas: "textures_04"},
                {name: "box03", atlas: "textures"},
                {name: "box04", atlas: "textures"}
            ],
            leftWall: {name: "wall_02_left", atlas: "textures_02", offset: -14},
            rightWall: {name: "wall_02_right", atlas: "textures_02", offset: -30},
            ground: {name: "ground_bg_plane", atlas: "textures_02"},
        },
        clawSpeed: 1,
        timeOfNextBox: 3500,
        nextBoxOffsetTime: 600,
        minimumTime: 1100,
        scoreForEveryDroppedBox: 150,
        scoreRow: 1500,
        scorePerItemUse: 2000,
        levelCompleteScore: "-",
        startBoard: {
            BOX: [
                {x: 3, y: 7},
                {x: 7, y: 7},
                {x: 7, y: 5},
            ],
            BRICK: [
                {x: 7, y: 6},
            ],
            EXPLOSION: []
        },
        spawnBoxes: [
            {type: SYMBOL.BOX, count: 60},
            {type: SYMBOL.EXPLOSION, count: 3},
            {type: SYMBOL.BRICK, count: 5}
        ],
        itemChance: 100,
        chancesPerItem: {
            lawnmower: 3,
            boxing: 1,
            springs: 2,
            hammer: 2,
            umbrella: 3
        }
    },

];
life = {value: 3, max: 3};



