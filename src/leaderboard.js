GameManager.Leaderboard.prototype = {

    create: function () {
        this.initEnvironment();
        this.initCredits();
        let self = this;
        let leaderboardData = null;
        if (_bc.brainCloudClient.authentication.anonymousId === "") {
            self.brainCloudCreateUser(function () {
                self.brainCloudGetLeaderboard(function () {
                    self.buildFrames(self);
                    self.tweenGroup();
                });
            });
        } else {
            self.brainCloudGetLeaderboard(function () {
                self.buildFrames(self);
                self.tweenGroup();
            });
        }
    },
    brainCloudGetLeaderboard: function (onComplete) {
        let leaderboardId = "score_leaderboard";
        let sortOrder = "HIGH_TO_LOW";
        let startIndex = 0;
        let endIndex = 2;
        let self = this;
        _bc.socialLeaderboard.getGlobalLeaderboardPage(leaderboardId, sortOrder, startIndex, endIndex, function (result) {
            var status = result.status;
            if (status === 200) {
                self.leaderboardData = result.data.leaderboard;
                if (onComplete) {
                    onComplete();
                }
            } else {
                console.error(status + " : " + JSON.stringify(result, null, 2));
                if (onComplete) {
                    onComplete();
                }
            }
        });
    },
    brainCloudCreateUser: function (onComplete) {
        currentScore = 1;
        var self = this;

        // _bc.brainCloudClient.authentication.anonymousId = "1234567890"; // re-use an Anon id
        // _bc.brainCloudClient.authentication.profileId = "0bfc43c6-a560-42ab-97ad-02d732f00630"; // re-use an Anon id

        _bc.brainCloudClient.authentication.anonymousId = _bc.brainCloudClient.authentication.generateAnonymousId()
        _bc.brainCloudClient.authentication.authenticateAnonymous(true,
            function (result) {
                if (result.status == 200) {
                    _bc.playerState.updateUserName(userName, function (result) {
                        var status = result.status;
                        if (status == 200) {
                            if (onComplete) {
                                onComplete();
                            }
                        } else {
                            console.error(status + " : " + JSON.stringify(result, null, 2));
                            if (onComplete) {
                                onComplete();
                            }
                        }
                    });

                } else {
                    var status = result.status;
                    console.error(status + " : " + JSON.stringify(result, null, 2));
                    if (onComplete) {
                        onComplete();
                    }
                }
            });

    },
    buildFrames: function (self) {
        player_1_group = game.add.group();
        player_2_group = game.add.group();
        player_3_group = game.add.group();
        this.buildGroup(player_1_group, 1, self);
        this.buildGroup(player_2_group, 2, self);
        this.buildGroup(player_3_group, 3, self);
        player_1_group.position.set(0, 10);
        player_2_group.position.set(320, 0);
        player_3_group.position.set(640, 0);
        player_1_group.angle = -2;
        player_3_group.angle = 2;

        allGroup = game.add.group();
        allGroup.add(player_1_group);
        allGroup.add(player_2_group);
        allGroup.add(player_3_group);
        allGroup.position.set(-1500 + 30, 310);
    },
    buildGroup: function (group, index, self) {
        group.add(game.add.sprite(0, 0, 'leaderboard', 'bottom'));
        group.add(game.add.sprite(0, 0, 'leaderboard', 'hair_' + getRandomInt(1, 5)));
        group.add(game.add.sprite(0, 0, 'leaderboard', 'shirt_' + getRandomInt(1, 4)));
        group.add(game.add.sprite(0, 0, 'leaderboard', 'mouth_' + getRandomInt(1, 4)));
        group.add(game.add.sprite(0, 0, 'leaderboard', 'nose_' + getRandomInt(1, 5)));
        group.add(game.add.sprite(0, 0, 'leaderboard', 'eyes_' + getRandomInt(1, 5)));
        group.add(game.add.sprite(0, 0, 'leaderboard', 'hands_' + index));
        group.add(game.add.sprite(0, 0, 'leaderboard', 'frame'));
        group.scale.set(0.86, 0.86);
        var textScore = game.add.text(166, group.height+40, self.leaderboardData[index-1].score, {
            font: '40px monospace',
            fill: '#000000'
        });
        textScore.anchor.set(0.5, 0.5);
        textScore.style.align = 'center';
        group.add(textScore);
        var textPlayer = game.add.text(166, group.height-38, self.leaderboardData[index-1].data.nickname, {
            font: '28px monospace',
            fill: '#000000'
        });
        textPlayer.anchor.set(0.5, 0.5);
        textPlayer.style.align = 'center';
        group.add(textPlayer);

    },
    buttonsOut: function (buttonIndex) {
        back_button_group.getChildAt(3).inputEnabled = false;
        back_button_group.getChildAt(3).input.useHandCursor = false;

        setTimeout(function () {
            PlaySound(chainSound);
        }, 100)

        game.add.tween(back_button_group).to({angle: 10}, 300, Phaser.Easing.Linear.None, true, 100);
        game.add.tween(back_button_group).to({x: '+1800'}, 2600, Phaser.Easing.Linear.None, true, 100).onComplete.add(function () {
            switch (buttonIndex) {
                case 0: {
                    game.state.start("Menu");
                    break;
                }
            }
        }, this);

        setTimeout(function () {
            PlaySound(chainSound);
        }, 400)
        game.add.tween(leaderboardBoard).to({x: '+1800'}, 2600, Phaser.Easing.Linear.None, true, 100);
        game.add.tween(allGroup).to({x: '+1800'}, 2600, Phaser.Easing.Linear.None, true, 100);
    },
    creditsHandler: function () {
        PlaySound(tapeSound);
        var graphics = game.add.graphics();
        graphics.beginFill(0x000000);
        graphics.drawRect(0, 0, game.world.width, game.world.height);
        graphics.endFill();
        graphics.alpha = 0;
        graphics.inputEnabled = true;
        graphics.input.useHandCursor = true;

        var creditsText = game.add.text(game.world.centerX, game.world.centerY + 50, '', {
            font: '40px monospace',
            fill: '#ffffff'
        });
        creditsText.text = creditsTextValue;
        creditsText.alpha = 0;
        creditsText.anchor.set(0.5, 0);
        creditsText.style.align = 'center';
        creditsText.y = 200;
        var self = this;
        game.add.tween(graphics).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true);
        game.add.tween(creditsText).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true).onComplete.add(function () {
            self.creditsTextMove(creditsText);
        });
        graphics.events.onInputDown.add(function () {
            game.add.tween(graphics).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
            game.add.tween(creditsText).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true).onComplete.add(function () {
                graphics.destroy();
            });
        }, this);
    },
    creditsTextMove: function (creditsText) {
        var self = this;
        game.add.tween(creditsText).to({y: -creditsText.height}, 30000, Phaser.Easing.Linear.None, true).onComplete.add(function () {
            creditsText.y = game.world.height;
            self.creditsTextMove(creditsText);
        });
    },
    initCredits: function () {
        credits = game.add.sprite(game.world.centerX + 540, 74, 'textures', 'credits');
        credits.anchor.set(0, 1);
        credits.scale.set(0.5, 0.5);
        credits.position.set(40, game.world.height - 30);
        credits.inputEnabled = true;
        credits.input.useHandCursor = true;
        var self = this;
        credits.events.onInputDown.add(function () {
            self.creditsHandler();
        }, this);

    },
    tweenGroup: function () {
        setTimeout(function () {
            PlaySound(chainSound);
        }, 100)
        game.add.tween(back_button_group).to({x: 1535 - 30 - (back_button_group.width / 2) + back_button_group.width / 2}, 2000, Phaser.Easing.Linear.None, true, 100).onComplete.add(function () {
            game.add.tween(back_button_group).to({angle: -10}, 300, Phaser.Easing.Back.Out, true).onComplete.add(function () {
                game.add.tween(back_button_group).to({angle: 0}, 1000, Phaser.Easing.Elastic.Out, true).onComplete.add(function () {
                    back_button_group.getChildAt(3).inputEnabled = true;
                    back_button_group.getChildAt(3).input.useHandCursor = true;
                }, this);
            }, this);
        }, this);

        game.add.tween(allGroup).to({x: 250 + 30}, 2000, Phaser.Easing.Linear.None, true, 300).onComplete.add(function () {
            game.add.tween(allGroup).to({x: 200 + 30}, 200, Phaser.Easing.Linear.None, true);
        }, this);


        setTimeout(function () {
            PlaySound(chainSound);
        }, 600)
        game.add.tween(leaderboardBoard).to({x: 250}, 2000, Phaser.Easing.Linear.None, true, 300).onComplete.add(function () {
            game.add.tween(leaderboardBoard).to({x: 200}, 200, Phaser.Easing.Linear.None, true);
        }, this);
    },
    initEnvironment: function () {
        background = game.add.sprite(0, 0, 'background');
        ground = game.add.sprite(0, game.world.height - 89, 'textures', 'ground');
        wallLeft = game.add.sprite(-12, 0, 'textures', 'wallLeft');
        wallRight = game.add.sprite(game.world.width - 74, 0, 'textures', 'wallRight');
        soundOn = game.add.sprite(game.world.width - 230, game.world.height - 130, 'textures', 'soundOn');
        soundOff = game.add.sprite(game.world.width - 230, game.world.height - 130, 'textures', 'soundOff');


        logo = game.add.sprite(game.world.width / 2, game.world.height / 2 + 140, 'logo');

        logo.anchor.set(0.5, 0.5);
        leaderboardBoard = game.add.sprite(-1500, 0, 'leaderboard', 'leaderboard');
        if (mute) {
            soundOff.inputEnabled = true;
            soundOff.input.useHandCursor = true;
            if (soundOn.inputEnabled != null) {
                soundOn.inputEnabled = false;
                soundOn.input.useHandCursor = false;
            }
            soundOn.alpha = 0;
            soundOff.alpha = 1;
        } else {
            soundOn.inputEnabled = true;
            soundOn.input.useHandCursor = true;
            if (soundOff.inputEnabled != null) {
                soundOff.inputEnabled = false;
                soundOff.input.useHandCursor = false;
            }
            soundOff.alpha = 0;
            soundOn.alpha = 1;
        }
        tapeSound = game.add.audio('tape');
        tapeSound.allowMultiple = true;

        soundOn.events.onInputDown.add(function () {
            tapeSound.play();
            mute = true;
            setMute();
            soundOn.inputEnabled = false;
            soundOn.input.useHandCursor = false;
            soundOff.inputEnabled = true;
            soundOff.input.useHandCursor = true;
            soundOn.alpha = 0;
            soundOff.alpha = 1;
        }, this);
        soundOff.events.onInputDown.add(function () {
            mute = false;
            PlaySound(tapeSound);
            setMute();
            soundOff.inputEnabled = false;
            soundOff.input.useHandCursor = false;
            soundOn.inputEnabled = true;
            soundOn.input.useHandCursor = true;
            soundOff.alpha = 0;
            soundOn.alpha = 1;
        }, this);


        light3 = game.add.sprite(game.world.centerX + 540, 74, 'textures', 'light');
        light3.anchor.set(0.5, 0);
        light3.alpha = 0;

        if (music.volume != 1) {
            music.volume = 0;
        }
        PlaySound(music);
        game.add.tween(music).to({volume: 1}, 1500, Phaser.Easing.Linear.None, true)

        chainSound = game.add.audio('menuIn');
        chainSound.allowMultiple = true;
        chainSound.volume = 0.3;

        back_button_group = game.add.group()

        back_button_group.add(game.add.sprite(0, 0, 'textures', 'clawBase'));
        back_button_group.add(game.add.sprite(-138, 124, 'textures', 'clawLeft'));
        back_button_group.add(game.add.sprite(98, 124, 'textures', 'clawRight'));
        back_button_group.add(game.add.sprite(-64, 230, 'textures', 'boxBack'));
        back_button_group.position.set(1506 - (back_button_group.width / 2), 0);

        back_button_group.pivot.x = back_button_group.width / 4 - 30;
        back_button_group.pivot.y = 20;
        back_button_group.position.x += back_button_group.width / 4 - 30;
        back_button_group.position.y += 10;


        lightOn = game.add.audio('lightOn');
        lightOff = game.add.audio('lightOff');
        lightOn.allowMultiple = true;
        lightOff.allowMultiple = true;
        back_button_group.getChildAt(3).events.onInputOver.add(function () {
            light3.alpha = 1;
            PlaySound(lightOn);
        }, this);
        back_button_group.getChildAt(3).events.onInputOut.add(function () {
            light3.alpha = 0;
            PlaySound(lightOff);
        }, this);


        back_button_group.position.x -= 1730;
        back_button_group.angle = 10;

        back_button_group.getChildAt(3).events.onInputDown.add(function () {
            back_button_group.getChildAt(3).inputEnabled = false;
            back_button_group.getChildAt(3).input.useHandCursor = false;
            this.buttonsOut(0);
        }, this);
    },
}