GameManager.Menu.prototype = {

    create: function () {

        this.initEnvironment();
        this.initSounds();
        this.initCredits();

        play_button_group = game.add.group();
        howTo_button_group = game.add.group();
        quit_button_group = game.add.group();

        this.initGroup(play_button_group, 430, 0, light1, 'boxPlay');
        this.initGroup(howTo_button_group, 980, 1, light2, 'boxHowToPlay');
        this.initGroup(quit_button_group, 1506, 2, light3, 'boxQuit');

        var self = this;

        this.tweenInGroup(play_button_group, 458, 500);
        this.tweenInGroup(howTo_button_group, 1006, 300);
        this.tweenInGroup(quit_button_group, 1535, 100);

    },
    creditsHandler: function () {
        PlaySound(tapeSound);
        var graphics = game.add.graphics();
        graphics.beginFill(0x000000);
        graphics.drawRect(0, 0, game.world.width, game.world.height);
        graphics.endFill();
        graphics.alpha = 0;
        graphics.inputEnabled = true;
        graphics.input.useHandCursor = true;

        var creditsText = game.add.text(game.world.centerX, game.world.centerY + 50, '', {
            font: '40px monospace',
            fill: '#ffffff'
        });
        creditsText.text = creditsTextValue;
        creditsText.alpha = 0;
        creditsText.anchor.set(0.5, 0);
        creditsText.style.align = 'center';
        creditsText.y = 200;
        var self = this;
        game.add.tween(graphics).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true);
        game.add.tween(creditsText).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true).onComplete.add(function () {
            self.creditsTextMove(creditsText);
        });
        graphics.events.onInputDown.add(function () {
            game.add.tween(graphics).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
            game.add.tween(creditsText).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true).onComplete.add(function () {
                graphics.destroy();
            });
        }, this);
    },
    creditsTextMove: function (creditsText) {
        var self = this;
        game.add.tween(creditsText).to({y: -creditsText.height}, 30000, Phaser.Easing.Linear.None, true).onComplete.add(function () {
            creditsText.y = game.world.height;
            self.creditsTextMove(creditsText);
        });
    },
    buttonsOut: function (buttonIndex) {

        if (quit_button_group.getChildAt(3).inputEnabled != null) {
            quit_button_group.getChildAt(3).inputEnabled = false;
            quit_button_group.getChildAt(3).input.useHandCursor = false;
        }
        if (howTo_button_group.getChildAt(3).inputEnabled != null) {
            howTo_button_group.getChildAt(3).inputEnabled = false;
            howTo_button_group.getChildAt(3).input.useHandCursor = false;
        }
        if (play_button_group.getChildAt(3).inputEnabled != null) {
            play_button_group.getChildAt(3).inputEnabled = false;
            play_button_group.getChildAt(3).input.useHandCursor = false;
        }
        setTimeout(function () {
            PlaySound(chainSound);
        }, 100)
        setTimeout(function () {
            PlaySound(chainSound);
        }, 300)
        setTimeout(function () {
            PlaySound(chainSound);
        }, 500)
        game.add.tween(quit_button_group).to({angle: 10}, 300, Phaser.Easing.Linear.None, true, 100);
        game.add.tween(quit_button_group).to({x: '+1800'}, 2000, Phaser.Easing.Linear.None, true, 100);
        game.add.tween(howTo_button_group).to({angle: 10}, 300, Phaser.Easing.Linear.None, true, 300);
        game.add.tween(howTo_button_group).to({x: '+1800'}, 2000, Phaser.Easing.Linear.None, true, 300);
        game.add.tween(play_button_group).to({angle: 10}, 300, Phaser.Easing.Linear.None, true, 500);
        game.add.tween(play_button_group).to({x: '+1800'}, 2000, Phaser.Easing.Linear.None, true, 500).onComplete.add(() => {
            switch (buttonIndex) {
                case 0: {
                    this.startGame();
                    break;
                }
                case 1: {
                    game.state.start("HowTo");
                    break;
                }
                case 2: {
                    game.state.start("Leaderboard");
                    break;
                }
            }
        }, this);
    },
    initEnvironment: function () {
        background = game.add.sprite(0, 0, 'background');
        ground = game.add.sprite(0, game.world.height - 89, 'textures', 'ground');
        wallLeft = game.add.sprite(-12, 0, 'textures', 'wallLeft');
        wallRight = game.add.sprite(game.world.width - 74, 0, 'textures', 'wallRight');

        logo = game.add.sprite(game.world.width / 2, game.world.height / 2 + 140, 'logo');
        logo.anchor.set(0.5, 0.5);


        light1 = game.add.sprite(game.world.centerX - 534, 76, 'textures', 'light');
        light2 = game.add.sprite(game.world.centerX + 14, 76, 'textures', 'light');
        light3 = game.add.sprite(game.world.centerX + 540, 74, 'textures', 'light');
        light1.anchor.set(0.5, 0);
        light2.anchor.set(0.5, 0);
        light3.anchor.set(0.5, 0);
        light1.alpha = 0;
        light2.alpha = 0;
        light3.alpha = 0;
    },
    initSounds: function () {
        soundOn = game.add.sprite(game.world.width - 230, game.world.height - 130, 'textures', 'soundOn');
        soundOff = game.add.sprite(game.world.width - 230, game.world.height - 130, 'textures', 'soundOff');
        soundOn.alpha = 1;
        soundOff.alpha = 0;
        soundOn.inputEnabled = true;
        soundOn.input.useHandCursor = true;
        if (mute) {
            soundOff.inputEnabled = true;
            soundOff.input.useHandCursor = true;
            if (soundOn.inputEnabled != null) {
                soundOn.inputEnabled = false;
                soundOn.input.useHandCursor = false;
            }
            soundOn.alpha = 0;
            soundOff.alpha = 1;
        } else {
            soundOn.inputEnabled = true;
            soundOn.input.useHandCursor = true;
            if (soundOff.inputEnabled != null) {
                soundOff.inputEnabled = false;
                soundOff.input.useHandCursor = false;
            }
            soundOff.alpha = 0;
            soundOn.alpha = 1;
        }

        tapeSound = game.add.audio('tape');
        tapeSound.allowMultiple = true;

        soundOn.events.onInputDown.add(function () {
            tapeSound.play();
            mute = true;
            setMute();
            soundOn.inputEnabled = false;
            soundOn.input.useHandCursor = false;
            soundOff.inputEnabled = true;
            soundOff.input.useHandCursor = true;
            soundOn.alpha = 0;
            soundOff.alpha = 1;
        }, this);
        soundOff.events.onInputDown.add(function () {
            mute = false;
            PlaySound(tapeSound);
            setMute();
            soundOff.inputEnabled = false;
            soundOff.input.useHandCursor = false;
            soundOn.inputEnabled = true;
            soundOn.input.useHandCursor = true;
            soundOff.alpha = 0;
            soundOn.alpha = 1;
        }, this);

        if (music.volume != 1) {
            music.volume = 0;
        }
        PlaySound(music);
        game.add.tween(music).to({volume: 1}, 1500, Phaser.Easing.Linear.None, true)

        chainSound = game.add.audio('menuIn');
        chainSound.allowMultiple = true;
        chainSound.volume = 0.3;

        lightOn = game.add.audio('lightOn');
        lightOff = game.add.audio('lightOff');
        lightOn.allowMultiple = true;
        lightOff.allowMultiple = true;
    },
    initCredits: function () {
        credits = game.add.sprite(game.world.centerX + 540, 74, 'textures', 'credits');
        credits.anchor.set(0, 1);
        credits.scale.set(0.5, 0.5);
        credits.position.set(40, game.world.height - 30);
        credits.inputEnabled = true;
        credits.input.useHandCursor = true;
        var self = this;
        credits.events.onInputDown.add(function () {
            self.creditsHandler();
        }, this);

    },
    initGroup: function (group, startPos, indexScreen, light, textureBox) {
        group.add(game.add.sprite(0, 0, 'textures', 'clawBase'));
        group.add(game.add.sprite(-138, 124, 'textures', 'clawLeft'));
        group.add(game.add.sprite(98, 124, 'textures', 'clawRight'));
        group.add(game.add.sprite(-64, 230, 'textures', textureBox));
        group.position.set(startPos - (group.width / 2), 0);

        group.pivot.x = group.width / 4 - 30;
        group.pivot.y = 20;
        group.position.x += group.width / 4 - 30;
        group.position.y += 10;

        group.position.x -= 1730;
        group.angle = 10;

        group.getChildAt(3).events.onInputDown.add(function () {
            group.getChildAt(3).inputEnabled = false;
            group.getChildAt(3).input.useHandCursor = false;
            this.buttonsOut(indexScreen);
        }, this);

        group.getChildAt(3).events.onInputOver.add(function () {
            light.alpha = 1;
            PlaySound(lightOn);
        }, this);
        group.getChildAt(3).events.onInputOut.add(function () {
            light.alpha = 0;
            PlaySound(lightOff);
        }, this);
    },
    tweenInGroup: function (group, pos, time) {

        setTimeout(function () {
            PlaySound(chainSound);
        }, time);
        game.add.tween(group).to({x: pos - 30 - (group.width / 2) + group.width / 2}, 2000, Phaser.Easing.Linear.None, true, time).onComplete.add(function () {
            game.add.tween(group).to({angle: -10}, 300, Phaser.Easing.Back.Out, true).onComplete.add(function () {
                game.add.tween(group).to({angle: 0}, 1000, Phaser.Easing.Elastic.Out, true).onComplete.add(function () {
                    group.getChildAt(3).inputEnabled = true;
                    group.getChildAt(3).input.useHandCursor = true;
                }, this)
            }, this)
        }, this);
    },
    startGame: function () {
        game.add.tween(music).to({volume: 0}, 400, Phaser.Easing.Linear.None, true);
        var graphics = game.add.graphics();
        graphics.beginFill(0x000000);
        graphics.drawRect(0, 0, game.world.width, game.world.height);
        graphics.endFill();
        graphics.alpha = 0;
        game.add.tween(graphics).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true).onComplete.add(function () {
            music.stop();
            game.state.start("NewGame");
        });
    }
};