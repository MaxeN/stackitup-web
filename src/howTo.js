GameManager.HowTo.prototype = {

    create: function () {

        background = game.add.sprite(0, 0, 'background');
        ground = game.add.sprite(0, game.world.height - 89, 'textures', 'ground');
        wallLeft = game.add.sprite(-12, 0, 'textures', 'wallLeft');
        wallRight = game.add.sprite(game.world.width - 74, 0, 'textures', 'wallRight');
        soundOn = game.add.sprite(game.world.width - 230, game.world.height - 130, 'textures', 'soundOn');
        soundOff = game.add.sprite(game.world.width - 230, game.world.height - 130, 'textures', 'soundOff');


        logo = game.add.sprite(game.world.width / 2, game.world.height / 2 + 140, 'logo');

        logo.anchor.set(0.5, 0.5);


        table = game.add.group();
        table.x = -1500;
        currentIndex = 0;
        howToArray = [];
        howToArray.push(game.add.sprite(0, 0, 'textures_03', 'how_to_play0'));
        howToArray.push(game.add.sprite(0, 0, 'textures_03', 'how_to_play1'));
        howToArray.push(game.add.sprite(0, 0, 'textures_03', 'how_to_play2'));
        table.add(howToArray[0]);
        table.add(howToArray[1]);
        table.add(howToArray[2]);
        arrow_right = game.add.sprite(898, 762, 'textures_03', 'arrow_right');
        arrow_left = game.add.sprite(-46, 762, 'textures_03', 'arrow_left');
        arrow_right.events.onInputDown.add(() => {
            PlaySound(lightOff);
            this.nextHelp();

        }, this);
        arrow_left.events.onInputDown.add(() => {
            PlaySound(lightOff);
            this.previousHelp();
        }, this);
        arrow_right.events.onInputOver.add(function () {
            PlaySound(lightOn);
        }, this);
        arrow_left.events.onInputOver.add(function () {
            PlaySound(lightOn);
        }, this);


        if (mute) {
            soundOff.inputEnabled = true;
            soundOff.input.useHandCursor = true;
            if (soundOn.inputEnabled != null) {
                soundOn.inputEnabled = false;
                soundOn.input.useHandCursor = false;
            }
            soundOn.alpha = 0;
            soundOff.alpha = 1;
        } else {
            soundOn.inputEnabled = true;
            soundOn.input.useHandCursor = true;
            if (soundOff.inputEnabled != null) {
                soundOff.inputEnabled = false;
                soundOff.input.useHandCursor = false;
            }
            soundOff.alpha = 0;
            soundOn.alpha = 1;
        }
        tapeSound = game.add.audio('tape');
        tapeSound.allowMultiple = true;

        soundOn.events.onInputDown.add(function () {
            tapeSound.play();
            mute = true;
            setMute();
            soundOn.inputEnabled = false;
            soundOn.input.useHandCursor = false;
            soundOff.inputEnabled = true;
            soundOff.input.useHandCursor = true;
            soundOn.alpha = 0;
            soundOff.alpha = 1;
        }, this);
        soundOff.events.onInputDown.add(function () {
            mute = false;
            PlaySound(tapeSound);
            setMute();
            soundOff.inputEnabled = false;
            soundOff.input.useHandCursor = false;
            soundOn.inputEnabled = true;
            soundOn.input.useHandCursor = true;
            soundOff.alpha = 0;
            soundOn.alpha = 1;
        }, this);

        credits = game.add.sprite(game.world.centerX + 540, 74, 'textures', 'credits');
        credits.anchor.set(0, 1);
        credits.scale.set(0.5, 0.5);
        credits.position.set(40, game.world.height - 30);
        credits.inputEnabled = true;
        credits.input.useHandCursor = true;
        var self = this;
        credits.events.onInputDown.add(function () {
            self.creditsHandler();
        }, this);


        light3 = game.add.sprite(game.world.centerX + 540, 74, 'textures', 'light');
        light3.anchor.set(0.5, 0);
        light3.alpha = 0;

        if (music.volume != 1) {
            music.volume = 0;
        }
        PlaySound(music);
        game.add.tween(music).to({volume: 1}, 1500, Phaser.Easing.Linear.None, true)

        chainSound = game.add.audio('menuIn');
        chainSound.allowMultiple = true;
        chainSound.volume = 0.3;

        back_button_group = game.add.group()

        back_button_group.add(game.add.sprite(0, 0, 'textures', 'clawBase'));
        back_button_group.add(game.add.sprite(-138, 124, 'textures', 'clawLeft'));
        back_button_group.add(game.add.sprite(98, 124, 'textures', 'clawRight'));
        back_button_group.add(game.add.sprite(-64, 230, 'textures', 'boxBack'));
        back_button_group.position.set(1506 - (back_button_group.width / 2), 0);

        back_button_group.pivot.x = back_button_group.width / 4 - 30;
        back_button_group.pivot.y = 20;
        back_button_group.position.x += back_button_group.width / 4 - 30;
        back_button_group.position.y += 10;


        lightOn = game.add.audio('lightOn');
        lightOff = game.add.audio('lightOff');
        lightOn.allowMultiple = true;
        lightOff.allowMultiple = true;
        back_button_group.getChildAt(3).events.onInputOver.add(function () {
            light3.alpha = 1;
            PlaySound(lightOn);
        }, this);
        back_button_group.getChildAt(3).events.onInputOut.add(function () {
            light3.alpha = 0;
            PlaySound(lightOff);
        }, this);


        back_button_group.position.x -= 1730;
        back_button_group.angle = 10;

        back_button_group.getChildAt(3).events.onInputDown.add(function () {
            back_button_group.getChildAt(3).inputEnabled = false;
            back_button_group.getChildAt(3).input.useHandCursor = false;
            this.buttonsOut(() => {
                game.state.start("Menu");
            });
        }, this);
        this.addCurrentHelp();
        this.buttonsIn(() => {
            back_button_group.getChildAt(3).inputEnabled = true;
            back_button_group.getChildAt(3).input.useHandCursor = true;
            this.unblockArrows();
        })
    },
    nextHelp: function () {
        this.blockArrows();
        this.buttonsOut(() => {
            if (++currentIndex >= howToArray.length) {
                currentIndex = 0;
            }
            this.addCurrentHelp();
            this.buttonsIn(() => {
                back_button_group.getChildAt(3).inputEnabled = true;
                back_button_group.getChildAt(3).input.useHandCursor = true;
                this.unblockArrows();

            })
        });
    },
    previousHelp: function () {
        this.blockArrows();
        this.buttonsOut(() => {
            if (--currentIndex < 0) {
                currentIndex = howToArray.length - 1;
            }
            this.addCurrentHelp();
            this.buttonsIn(() => {
                back_button_group.getChildAt(3).inputEnabled = true;
                back_button_group.getChildAt(3).input.useHandCursor = true;
                this.unblockArrows();

            })
        });
    },
    blockArrows: function () {
        arrow_left.inputEnabled = false;
        arrow_left.input.useHandCursor = false;
        arrow_right.inputEnabled = false;
        arrow_right.input.useHandCursor = false;
    },
    unblockArrows: function () {
        arrow_left.inputEnabled = true;
        arrow_left.input.useHandCursor = true;
        arrow_right.inputEnabled = true;
        arrow_right.input.useHandCursor = true;
    },
    addCurrentHelp: function () {
        table.removeAll();
        table.add(howToArray[currentIndex]);
        table.add(arrow_left);
        table.add(arrow_right);
    },
    buttonsIn: function (onComplete) {
        setTimeout(function () {
            PlaySound(chainSound);
        }, 100)
        game.add.tween(back_button_group).to({x: 1535 - 30 - (back_button_group.width / 2) + back_button_group.width / 2}, 2000, Phaser.Easing.Linear.None, true, 100).onComplete.add(function () {
            game.add.tween(back_button_group).to({angle: -10}, 300, Phaser.Easing.Back.Out, true).onComplete.add(function () {
                game.add.tween(back_button_group).to({angle: 0}, 700, Phaser.Easing.Elastic.Out, true).onComplete.add(function () {
                    onComplete();
                }, this);
            }, this);
        }, this);


        setTimeout(function () {
            PlaySound(chainSound);
        }, 600)
        game.add.tween(table).to({x: 250}, 2000, Phaser.Easing.Linear.None, true, 300).onComplete.add(function () {
            game.add.tween(table).to({x: 200}, 200, Phaser.Easing.Linear.None, true);
        }, this);
    },
    buttonsOut: function (onComplete) {
        back_button_group.getChildAt(3).inputEnabled = false;
        back_button_group.getChildAt(3).input.useHandCursor = false;

        setTimeout(function () {
            PlaySound(chainSound);
        }, 100)

        game.add.tween(back_button_group).to({angle: 10}, 300, Phaser.Easing.Linear.None, true, 100);
        game.add.tween(back_button_group).to({x: '+1800'}, 2600, Phaser.Easing.Linear.None, true, 100).onComplete.add(function () {
            back_button_group.position.set(1506 - (back_button_group.width / 2), 0);
            back_button_group.position.x += back_button_group.width / 4 - 30;
            back_button_group.position.x -= 1730;
            back_button_group.angle = 10;
            table.x = -1500;
            onComplete();

        }, this);

        setTimeout(function () {
            PlaySound(chainSound);
        }, 400)

        game.add.tween(table).to({x: '+1800'}, 2600, Phaser.Easing.Linear.None, true, 100);
    },
    creditsHandler: function () {
        PlaySound(tapeSound);
        var graphics = game.add.graphics();
        graphics.beginFill(0x000000);
        graphics.drawRect(0, 0, game.world.width, game.world.height);
        graphics.endFill();
        graphics.alpha = 0;
        graphics.inputEnabled = true;
        graphics.input.useHandCursor = true;

        var creditsText = game.add.text(game.world.centerX, game.world.centerY + 50, '', {
            font: '40px monospace',
            fill: '#ffffff'
        });
        creditsText.text = creditsTextValue;
        creditsText.alpha = 0;
        creditsText.anchor.set(0.5, 0);
        creditsText.style.align = 'center';
        creditsText.y = 200;
        var self = this;
        game.add.tween(graphics).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true);
        game.add.tween(creditsText).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true).onComplete.add(function () {
            self.creditsTextMove(creditsText);
        });
        graphics.events.onInputDown.add(function () {
            game.add.tween(graphics).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
            game.add.tween(creditsText).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true).onComplete.add(function () {
                graphics.destroy();
            });
        }, this);
    },
    creditsTextMove: function (creditsText) {
        var self = this;
        game.add.tween(creditsText).to({y: -creditsText.height}, 30000, Phaser.Easing.Linear.None, true).onComplete.add(function () {
            creditsText.y = game.world.height;
            self.creditsTextMove(creditsText);
        });
    },

}